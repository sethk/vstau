#if !__x86_64__
#ifndef __VSTAUCARBONVIEW__H_
#define __VSTAUCARBONVIEW__H_

#include "AUCarbonViewBase.h"

class VSTAUCarbonView : public AUCarbonViewBase
{
public:
	VSTAUCarbonView(AudioUnitCarbonView view);
	virtual ~VSTAUCarbonView(void);

	virtual OSStatus CreateUI(Float32 inXOffset, Float32 inYOffset);

protected:
	static void IdleCall(EventLoopTimerRef inTimer, void *inUserData);

	EventLoopTimerRef mIdleTimer;

private:
	UInt16 CountOfSubviews(ControlRef view);
	ControlRef ContentView(void);
	CFArrayRef GetSubviews(HIViewRef view);
	ControlRef FindNewSubview(ControlRef view, CFArrayRef oldSubviews);

	AEffect *mEffect;
};

#endif // __VSTAUCARBONVIEW__H_
#endif // !__x86_64__