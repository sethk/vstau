//
//  VMStatusFormatter.m
//  VSTAU
//
//  Created by Seth Kingsley on 11/25/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import	"VMPlugIn.h"
#import "VMStatusFormatter.h"

static const NSString *kErrorString;
static const NSString *kUninstalledString;
static const NSString *kAUExistsString;
static const NSString *kInstalledString;

@implementation VMStatusFormatter

+ (void)initialize
{
	kErrorString = NSLocalizedString(@"Error", nil);
	kUninstalledString = NSLocalizedString(@"Uninstalled", nil);
	kAUExistsString = NSLocalizedString(@"AU Exists", nil);
	kInstalledString = NSLocalizedString(@"Installed", nil);
}

- (NSString *)stringForObjectValue:(id)object
{
	switch ((enum VMPlugInStatus)[object intValue])
	{
		case kStatusError: return kErrorString;
		case kStatusUninstalled: return kUninstalledString;
		case kStatusAUExists: return kAUExistsString;
		case kStatusInstalled: return kInstalledString;
		default: return @"<Invalid Enum>";
	}
}

@end
