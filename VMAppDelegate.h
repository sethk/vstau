//
//  VMAppDelegate.h
//  VSTAU
//
//  Created by Seth Kingsley on 11/24/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import	"VMProgress.h"

@class VMPlugInsController;

@interface VMAppDelegate : NSObject<NSApplicationDelegate>
{
@public
	IBOutlet VMPlugInsController *_plugInsController;
	IBOutlet NSWindow *_managerWindow;
	IBOutlet NSPanel *progressPanel;
	IBOutlet NSProgressIndicator *progressIndicator;

@protected
	BOOL _ranFirstRefresh;
	VMProgress *progress;
	NSString *_progressTitle;
	NSArray *plugIns;
	unsigned numChangingPlugIns, numChangedPlugIns;
}

// Accessors:
- (VMProgress *)progress;
@property (copy, readonly, nonatomic) NSString *progressTitle;
- (NSArray *)plugIns;

// Actions:
- (IBAction)refresh:(id)sender;
- (IBAction)install:(id)sender;
- (IBAction)installAll:(id)sender;
- (IBAction)uninstall:(id)sender;
- (IBAction)uninstallAll:(id)sender;

@end
