//
//  VMPlugInScanner.m
//  VSTAU
//
//  Created by Seth Kingsley on 12/4/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import	"VMPlugIn.h"
#import "VMPlugInScanner.h"

static NSString	*kPlugInSystemPath = @"/Library/Audio/Plug-Ins/VST";
static NSString	*kPlugInUserPath = @"~/Library/Audio/Plug-Ins/VST";
static NSString	*kPlugInExtension = @"vst";

static const float kDirsProgress = 0.1;

@interface VMPlugInScanner (PrivateAPI)

- (NSArray *)scanDirectoryPaths:(NSArray *)dirPaths;
- (void)scanForPlugInPaths:(NSMutableArray *)plugInPaths inDirectoryAtPath:(NSString *)dirPath;
- (void)scanForPlugInsAtPaths:(NSArray *)plugInPaths;
- (void)setProgressAmount:(float)newProgressAmount;
- (void)setScannedPlugIns:(NSArray *)newScannedPlugIns;

@end

@implementation VMPlugInScanner (PrivateAPI)

- (NSArray *)scanDirectoryPaths:(NSArray *)dirPaths
{
	NSMutableArray *plugInPaths = [NSMutableArray array];
	NSEnumerator *dirEnm = [dirPaths objectEnumerator];
	NSString *dirPath;
	unsigned numScannedDirs = 0, numPlugInDirs = [dirPaths count];
	while ((dirPath = [[dirEnm nextObject] stringByExpandingTildeInPath]))
	{
		[self scanForPlugInPaths:plugInPaths inDirectoryAtPath:dirPath];
		[self setProgressAmount:((float)++numScannedDirs / numPlugInDirs) * kDirsProgress];
	}

	return plugInPaths;
}

- (void)scanForPlugInPaths:(NSMutableArray *)plugInPaths inDirectoryAtPath:(NSString *)dirPath
{
	if (delegate && [delegate respondsToSelector:@selector(scanner:willScanDirectoryAtPath:)])
		[delegate scanner:self willScanDirectoryAtPath:dirPath];

	NSEnumerator *enm = [[NSFileManager defaultManager] enumeratorAtPath:dirPath];
	NSString *plugInFileName;
	while ((plugInFileName = [enm nextObject]))
		if ([[plugInFileName pathExtension] isEqualToString:kPlugInExtension])
			[plugInPaths addObject:[dirPath stringByAppendingPathComponent:plugInFileName]];

	if (delegate && [delegate respondsToSelector:@selector(scanner:didScanDirectoryAtPath:)])
		[delegate scanner:self didScanDirectoryAtPath:dirPath];
}

- (void)scanForPlugInsAtPaths:(NSArray *)plugInPaths
{
	float oldProgress = [self progressAmount], progressLeft = 1.0 - oldProgress;
	unsigned numScannedPlugIns = 0, numPlugInPaths = [plugInPaths count];
	NSEnumerator *plugInPathEnm = [plugInPaths objectEnumerator];
	NSString *plugInPath;
	while ((plugInPath = [plugInPathEnm nextObject]))
	{
		if (delegate && [delegate respondsToSelector:@selector(scanner:willScanPlugInAtPath:)])
			[delegate scanner:self willScanPlugInAtPath:plugInPath];

		VMPlugIn *plugIn = [VMPlugIn plugInWithPath:plugInPath];

		[self setProgressAmount:oldProgress + ((float)++numScannedPlugIns / numPlugInPaths) * progressLeft];

		if (plugIn)
		{
			[[self mutableArrayValueForKey:@"scannedPlugIns"] addObject:plugIn];

			if (delegate && [delegate respondsToSelector:@selector(scanner:didScanPlugIn:atPath:)])
				[delegate scanner:self didScanPlugIn:plugIn atPath:plugInPath];
		}
		else if (delegate && [delegate respondsToSelector:@selector(scanner:failedToScanPlugInAtPath:)])
			[delegate scanner:self failedToScanPlugInAtPath:plugInPath];
	}
}

- (void)setProgressAmount:(float)newProgressAmount
{
	progressAmount = newProgressAmount;
}

- (void)setScannedPlugIns:(NSArray *)newScannedPlugIns
{
	if (scannedPlugIns != newScannedPlugIns)
	{
		[scannedPlugIns release];
		scannedPlugIns = [newScannedPlugIns mutableCopy];
	}
}

@end

@implementation VMPlugInScanner

+ (VMPlugInScanner *)scannerWithDelegate:(id)newDelegate
{
	return [[[self alloc] initWithDelegate:newDelegate] autorelease];
}

- initWithDelegate:(id)newDelegate
{
	if ((self = [self init]))
	{
		delegate = newDelegate;
	}

	return self;
}

- init
{
	if ((self = [super init]))
	{
		scannedPlugIns = [NSMutableArray new];
	}

	return self;
}

- (void)dealloc
{
	[scannedPlugIns release];

	[super dealloc];
}

- (float)progressAmount
{
	return progressAmount;
}

- (NSArray *)scannedPlugIns
{
	return scannedPlugIns;
}

- (IBAction)scan:(id)sender
{
	[self setProgressAmount:0.0];

	if (delegate && [delegate respondsToSelector:@selector(scannerWillBeginScanning:)])
		[delegate scannerWillBeginScanning:self];

	[scannedPlugIns removeAllObjects];

	NSArray *dirPaths = [NSArray arrayWithObjects:kPlugInSystemPath, kPlugInUserPath, nil];
	NSArray *plugInPaths = [self scanDirectoryPaths:dirPaths];
	[self scanForPlugInsAtPaths:plugInPaths];

	if (delegate && [delegate respondsToSelector:@selector(scannerDidFinishScanning:)])
		[delegate scannerDidFinishScanning:self];
}

@end
