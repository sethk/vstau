//
//  VMPlugIn.h
//  VSTAU
//
//  Created by Seth Kingsley on 11/24/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>

enum VMPlugInStatus
{
	kStatusError = -1,
	kStatusUninstalled,
	kStatusAUExists,
	kStatusInstalled
};

// I cannot, for the life of me, figure out how to make an NSPopUpButtonCell use a formatter, hence, I will wrap status
// values in objects and implement -stringValue.
@interface VMPlugInStatusObject : NSObject
{
@protected
	enum VMPlugInStatus value;
}

// Descriptive Strings:
+ (NSString *)verbForValue:(enum VMPlugInStatus)value;
+ (NSString *)progressiveVerbForValue:(enum VMPlugInStatus)value;
+ (NSString *)pastTenseVerbForValue:(enum VMPlugInStatus)value;

+ (VMPlugInStatusObject *)statusObjectWithValue:(enum VMPlugInStatus)newValue;
- initWithValue:(enum VMPlugInStatus)newValue;
- (enum VMPlugInStatus)statusValue;
- (NSString *)stringValue;

@end

@interface VMPlugInFormat : NSObject <NSCopying>
{
@protected
	unsigned numInputs, numOutputs;
}

+ (VMPlugInFormat *)formatWithNumInputs:(unsigned)newNumInputs numOutputs:(unsigned)newNumOutputs;
- initWithNumInputs:(unsigned)newNumInputs numOutputs:(unsigned)newNumOutputs;
- (unsigned)numInputs;
- (unsigned)numOutputs;
- (NSString *)stringValue;
- (NSComparisonResult)compare:(VMPlugInFormat *)otherFormat;

@end

@interface VMPlugIn : NSObject
{
@protected
	NSString *VSTPath;
	NSString *componentPath;
	NSString *componentBundleIdentifier;
	enum VMPlugInStatus status;
	NSString *errorString;
	OSType uniqueID;
	NSString *manufacturerName;
	NSString *productName;
	unsigned version;
	unsigned VSTVersion;
	BOOL isMIDI;
	VMPlugInFormat *format;
}

// Initializers:
+ (VMPlugIn *)plugInWithPath:(NSString *)newPath;
- initWithPath:(NSString *)newPath;

// Accessors:
- (enum VMPlugInStatus)status;
- (void)setStatus:(enum VMPlugInStatus)newStatus;
- (VMPlugInStatusObject *)statusObject;
- (void)setStatusObject:(VMPlugInStatusObject *)statusObject;
- (NSArray *)statusValues;
- (NSArray *)statusObjectValues;
- (NSString *)errorString;
- (OSType)uniqueID;
- (NSString *)manufacturerName;
- (NSString *)productName;
- (unsigned)version;
- (unsigned)VSTVersion;
- (BOOL)isMIDI;
- (VMPlugInFormat *)format;
- (NSString *)VSTPath;
- (NSString *)abbreviatedVSTPath;
- (NSString *)componentPath;
- (NSString *)componentBundleIdentifier;

@end
