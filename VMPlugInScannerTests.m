//
//  VMPlugInScannerTests.m
//  VSTAU
//
//  Created by Seth Kingsley on 12/29/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import "VMPlugInScannerTests.h"
#import	"VMPlugInScanner.h"

@implementation VMPlugInScannerTests

- (void)setUp
{
	scanner = [VMPlugInScanner new];
}

- (void)testScan
{
	[scanner scan:nil];
}

- (void)tearDown
{
	[scanner release];
}

@end
