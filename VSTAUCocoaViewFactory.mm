//
//  VSTAUCocoaViewFactory.m
//  VSTAU
//
//  Created by Seth Kingsley on 12/25/10.
//  Copyright 2010 Apple Inc. All rights reserved.
//

#import "VSTAUCocoaViewFactory.h"
#import "VSTAUDefs.h"
#include <pluginterfaces/vst2.x/aeffectx.h>
#include <iostream>

using namespace std;

@interface VSTAUCocoaView : NSView
{
	struct AEffect *_effect;
	NSTimer *_idleTimer;
}

- initWithFrame:(NSRect)frame effect:(struct AEffect *)effect;
- (void)_timerFired:(NSTimer *)timer;
- (void)_destroy;

@end

@implementation VSTAUCocoaView

- initWithFrame:(NSRect)frame effect:(struct AEffect *)effect
{
	if ((self = [super initWithFrame:frame]))
	{
		_effect = effect;
	}

	return self;
}

- (void)_timerFired:(NSTimer *)timer
{
	_effect->dispatcher(_effect, effEditIdle, 0, 0, 0, 0);
}

- (void)_destroy
{
	[_idleTimer invalidate];
	_idleTimer = nil;

	if (_effect)
	{
#ifdef DEBUG
		clog << "VSTAU: Closing editor..." << endl;
#endif // DEBUG
		_effect->dispatcher(_effect, effEditClose, 0, 0, 0, 0);
		_effect = NULL;

		[[self subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
	}
}

- (void)viewWillMoveToWindow:(NSWindow *)newWindow
{
	if (newWindow)
	{
		if (!_idleTimer && _effect)
		{
#ifdef DEBUG
			clog << "VSTAU: Starting idle timer..." << endl;
#endif // DEBUG
			_idleTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 / VSTAUIdleTimerFreq
														  target:self
														selector:@selector(_timerFired:)
														userInfo:nil
														 repeats:YES];
		}
	}
	else
		[self _destroy];
}

- (void)viewWillMoveToSuperview:(NSView *)newSuperview
{
	if (!newSuperview)
		[self _destroy];
}

- (void)dealloc
{
	[self _destroy];

	[super dealloc];
}

@end

@implementation VSTAUCocoaViewFactory

- (unsigned)interfaceVersion
{
	return 0;
}

- (NSString *)description
{
	return [NSString stringWithString:@"VSTAU Cocoa View"]; // Should be copy, not static instance
}

- (NSView *)uiViewForAudioUnit:(AudioUnit)inAudioUnit withSize:(NSSize)inPreferredSize
{
	// Get the VST through a custom parameter of the Audio Unit:
	struct AEffect *effect;
	UInt32 size = sizeof(effect);
	ComponentResult result = AudioUnitGetProperty(inAudioUnit,
			kVSTAUProperty_Effect, kAudioUnitScope_Global, 0,
			&effect, &size);
	BOOL shouldSizeToFit = NO;
	assert(result == noErr);

	// Determine the correct size:
	ERect editRect;
	if (effect->dispatcher(effect, effEditGetRect, 0, 0, &editRect, 0) &&
			editRect.top < editRect.bottom && editRect.left < editRect.right)
	{
		inPreferredSize.width = editRect.right - editRect.right;
		inPreferredSize.height = editRect.bottom - editRect.top;
	}
	if (inPreferredSize.width == 0.0 || inPreferredSize.height == 0.0)
	{
		inPreferredSize = NSMakeSize(200.0, 400.0);
		shouldSizeToFit = YES;
	}
	NSRect contentRect = {NSZeroPoint, inPreferredSize};

	// Open the effect UI:
	VSTAUCocoaView *VSTAUView = [[[VSTAUCocoaView alloc] initWithFrame:contentRect effect:effect] autorelease];
	effect->dispatcher(effect, effEditOpen, 0, 0, VSTAUView, 0);
	[VSTAUView retain];

	// Find the editor view:
	NSArray *subviews = [VSTAUView subviews];
	NSView *uiView = [subviews lastObject];
	if (!uiView)
	{
		NSLog(@"Cannot find editor in Cocoa view hierarchy");
		return nil;
	}
	[uiView retain];

	// Adjust the superview to fit if necessary:
	[VSTAUView setFrame:[uiView frame]];

#ifdef DEBUG
	clog << "VSTAU: Created view" << endl;
#endif // DEBUG
	
	return VSTAUView;
}

@end
