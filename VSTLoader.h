//
//  VSTLoader.h
//  VSTAU
//
//  Created by Seth Kingsley on 11/24/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#define VST_FORCE_DEPRECATED	0
#include <pluginterfaces/vst2.x/aeffectx.h>

#import	<Carbon/Carbon.h>

class VSTLoader
{
public:
	static ComponentResult LoadEffectBundle(const CFStringRef inPath,
			CFBundleRef &outBundle,
			CFStringRef &outErrorString);
	static ComponentResult CreateEffect(CFBundleRef inBundle, 
			audioMasterCallback inHostCallback,
			void *inUserData,
			AEffect *&outEffect,
			CFStringRef &outErrorString);
	static void DestroyEffect(AEffect *inEffect);
	static void UnloadEffectBundle(CFBundleRef inBundle);

	// Façade:
	static ComponentResult OpenEffect(const CFStringRef inPath,
			audioMasterCallback inHostCallback,
			void *inUserData,
			AEffect *&outEffect,
			CFBundleRef &outBundle,
			CFStringRef &outErrorString);
	static void CloseEffect(AEffect *inEffect, CFBundleRef inBundle);
};
