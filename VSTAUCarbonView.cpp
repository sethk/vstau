#if !__x86_64__
#include <pluginterfaces/vst2.x/aeffectx.h>

#include <iostream>
#include "VSTAU.h"
#include "VSTAUCarbonView.h"

using namespace std;

COMPONENT_ENTRY(VSTAUCarbonView)

VSTAUCarbonView::VSTAUCarbonView(AudioUnitCarbonView view)
	: AUCarbonViewBase(view)
{
}

VSTAUCarbonView::~VSTAUCarbonView(void)
{
	// Stop the idle timer:
	RemoveEventLoopTimer(mIdleTimer);

	// Close the editor:
	mEffect->dispatcher(mEffect, effEditClose, 0, 0, 0, 0);
}

OSStatus
VSTAUCarbonView::CreateUI(Float32 inXOffset, Float32 inYOffset)
{
	// Get the VST through a custom parameter of the Audio Unit:
	UInt32 size = sizeof(mEffect);
	ComponentResult result = AudioUnitGetProperty(mEditAudioUnit,
			kVSTAUProperty_Effect, kAudioUnitScope_Global, 0,
			&mEffect, &size);
	assert(result == noErr);

	// Save the subviews of the window so we can determine which one is new:
	ControlRef contentView = ContentView();
	CFArrayRef oldContentSubviews = VSTAUCarbonView::GetSubviews(contentView);

	// Open the editor:
	mEffect->dispatcher(mEffect, effEditOpen, 0, 0, mCarbonWindow, 0);

	// Find the editor view just added by the VST.
	ControlRef editorView = VSTAUCarbonView::FindNewSubview(contentView, oldContentSubviews);
	CFRelease(oldContentSubviews);
	if (!editorView)
	{
		clog << "Cannot find editor in view hierarchy" << endl;
		return kAudioUnitErr_FailedInitialization;
	}

	// Embed the editor in the real pane:
	if (IsCompositWindow())
	{
		HIViewRemoveFromSuperview(editorView);
		OSErr status = HIViewAddSubview(mCarbonPane, editorView);
		assert(status == noErr);
	}
	else
	{
		OSErr status = ::EmbedControl(editorView, mCarbonPane);
		assert(status == noErr);
	}

	// Resize the pane to the size of the editor:
	ERect *pRect;
	mEffect->dispatcher(mEffect, effEditGetRect, 0, 0, &pRect, 0);
	if (pRect)
	{
		/*if (isCompositWindow)
			HIViewSetFrame(...);
		else*/
			SizeControl(mCarbonPane, pRect->right - pRect->left, pRect->bottom - pRect-> top);
	}

	// Setup the idle timer:
	InstallEventLoopTimer(GetCurrentEventLoop(),
			kEventDurationSecond / VSTAUIdleTimerFreq, kEventDurationSecond / VSTAUIdleTimerFreq,
			VSTAUCarbonView::IdleCall, mEffect,
			&mIdleTimer);

	return noErr;
}

void
VSTAUCarbonView::IdleCall(EventLoopTimerRef inTimer, void *inUserData)
{
	AEffect *effect = (AEffect *)inUserData;
	effect->dispatcher(effect, effEditIdle, 0, 0, 0, 0);
}

UInt16
VSTAUCarbonView::CountOfSubviews(ControlRef view)
{
	if (IsCompositWindow())
		return HIViewCountSubviews(view);
	else
	{
		UInt16 countOfSubviews;
		OSErr status = CountSubControls(view, &countOfSubviews);
		assert(status == noErr);
		return countOfSubviews;
	}

}

ControlRef
VSTAUCarbonView::ContentView(void)
{
	ControlRef contentView = NULL;

	if (IsCompositWindow())
	{
		OSStatus status = HIViewFindByID(HIViewGetRoot(mCarbonWindow), kHIViewWindowContentID, &contentView);
		assert(status == noErr);
	}
	else
	{
		OSErr status = GetRootControl(mCarbonWindow, &contentView);
		assert(status == noErr);
	}

	return contentView;
}

CFArrayRef
VSTAUCarbonView::GetSubviews(ControlRef view)
{
	UInt16 countOfSubviews = VSTAUCarbonView::CountOfSubviews(view);
	CFMutableArrayRef subviews = CFArrayCreateMutable(NULL, countOfSubviews, NULL);

	if (IsCompositWindow())
	{
		HIViewRef subview = HIViewGetFirstSubview(view);
		while (subview)
		{
			CFArrayAppendValue(subviews, subview);
			subview = HIViewGetNextView(subview);
		}
	}
	else
	{
		for (UInt16 i = 0; i < countOfSubviews; ++i)
		{
			ControlRef subview;
			OSErr result = GetIndexedSubControl(view, i + 1, &subview);
			assert(result == noErr);
			CFArrayAppendValue(subviews, subview);
		}
	}

	return subviews;
}

HIViewRef
VSTAUCarbonView::FindNewSubview(HIViewRef view, CFArrayRef oldSubviews)
{
	// Assume that if count of subviews is the same, none were added:
	CFIndex oldCountOfSubviews = CFArrayGetCount(oldSubviews);
	UInt16 countOfSubviews = VSTAUCarbonView::CountOfSubviews(view);
	if (countOfSubviews == oldCountOfSubviews)
		return NULL;

	CFRange range = CFRangeMake(0, oldCountOfSubviews);
	if (IsCompositWindow())
	{
		HIViewRef subview = HIViewGetFirstSubview(view);
		while (subview)
		{
			if (!CFArrayContainsValue(oldSubviews, range, subview))
				return subview;
			subview = HIViewGetNextView(subview);
		}
	}
	else
	{
		for (UInt16 i = 0; i < countOfSubviews; ++i)
		{
			ControlRef subview;
			OSErr status = GetIndexedSubControl(view, i + 1, &subview);
			assert(status == noErr);
			if (!CFArrayContainsValue(oldSubviews, range, subview))
				return subview;
		}
	}

	return NULL;
}
#endif // !__x86_64__