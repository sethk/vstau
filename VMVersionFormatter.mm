//
//  VMVersionFormatter.mm
//  VSTAU
//
//  Created by Seth Kingsley on 11/25/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import "VMVersionFormatter.h"

@implementation VMVersionFormatter

- (NSString *)stringForObjectValue:(id)anObject
{
	int version = [anObject intValue];

	if (version <= 9999)
	{
		if (version % 100 == 0)
		{
			if (version != 0)
				return [NSString stringWithFormat:@"%i.%i", version / 1000, (version % 1000) / 100];
			else
				return @"";
		}
		else
			return [NSString stringWithFormat:@"%i.%i.%i", version / 1000, (version % 1000) / 100, version % 100];
	}
	else
		return [NSString stringWithFormat:@"%i", version];
}

@end
