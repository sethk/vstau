#if 1
#include <pluginterfaces/vst2.x/aeffectx.h>

#include "VSTAU.h"

COMPONENT_ENTRY(VSTAU);

VSTAU::VSTAU(AudioUnit component)
    : AUEffectBase(component)
{
    CFURLRef url = CFURLCreateWithFileSystemPath(NULL,
	    CFSTR("/Users/sethk/Library/Audio/Plug-Ins/VST/again.vst"),
	    kCFURLPOSIXPathStyle,
	    false);
    if (!url)
    {
	fprintf(stderr, "Invalid URL\n");
	throw(-1001);
    }
    bundle = CFBundleCreate(NULL, url);
    CFRelease(url);
    if (!bundle)
    {
	fprintf(stderr, "Could not load bundle\n");
	throw(-1002);
    }
    if (!CFBundleLoadExecutable(bundle))
    {
	fprintf(stderr, "Could not load bundle executable\n");
	throw(-1003);
    }

    CreateElements();

    AEffect *effect = GetEffect();
    Globals()->UseIndexedParameters(effect->numParams);
}

VSTAU::~VSTAU(void)
{
    if (cachedEffect)
	cachedEffect->dispatcher(cachedEffect, effClose, 0, 0, 0, 0);

    CFBundleUnloadExecutable(bundle);
    CFRelease(bundle);
}

#if 0
AUKernelBase *
VSTAU::NewKernel(void)
{
    VSTAUKernel *kernel = new VSTAUKernel(this, GetEffect());
    cachedEffect = NULL;

    return kernel;
}

ComponentResult
VSTAU::GetParameterValueStrings(AudioUnitScope inScope, AudioUnitParameterID inParameterID, CFArrayRef *outStrings)
{
    return kAudioUnitErr_InvalidProperty;
}

ComponentResult
VSTAU::GetParameterInfo(AudioUnitScope inScope,
	AudioUnitParameterID inParameterID,
	AudioUnitParameterInfo &outParameterInfo)
{
    if (inScope == kAudioUnitScope_Global)
    {
	AEffect *effect = GetEffect();
	outParameterInfo.flags = kAudioUnitParameterFlag_IsWritable | kAudioUnitParameterFlag_IsReadable;
	char paramName[kVstMaxParamStrLen];
	effect->dispatcher(effect, effGetParamName, inParameterID, 0, paramName, 0);
	AUBase::FillInParameterName(outParameterInfo,
		CFStringCreateWithCString(NULL, paramName, kCFStringEncodingMacRoman),
		true);
	outParameterInfo.unit = kAudioUnitParameterUnit_Generic;
	outParameterInfo.minValue = 0.0f;
	outParameterInfo.maxValue = 1.0f;
	outParameterInfo.defaultValue = 0.5;
	return noErr;
    }
    else
	return kAudioUnitErr_InvalidParameter;
}

ComponentResult
VSTAU::GetPropertyInfo(AudioUnitPropertyID inID,
	AudioUnitScope inScope,
	AudioUnitElement inElement,
	UInt32 &outDataSize,
	Boolean &outWritable)
{
    return AUEffectBase::GetPropertyInfo(inID, inScope, inElement, outDataSize, outWritable);
}

ComponentResult
VSTAU::GetProperty(AudioUnitPropertyID inID, AudioUnitScope inScope, AudioUnitElement inElement, void *outData)
{
    return AUEffectBase::GetProperty(inID, inScope, inElement, outData);
}

VSTAU::VSTAUKernel::VSTAUKernel(AUEffectBase *inAudioUnit, AEffect *inEffect)
    : AUKernelBase(inAudioUnit)
{
    effect = inEffect;
}

VSTAU::VSTAUKernel::~VSTAUKernel(void)
{
    effect->dispatcher(effect, effClose, 0, 0, 0, 0);
    delete effect;
}

void
VSTAU::VSTAUKernel::Reset(void)
{
}

void
VSTAU::VSTAUKernel::Process(const Float32 *inSourceP,
	Float32 *inDestP,
	UInt32 inFramesToProcess,
	UInt32 inNumChannels,
	bool &ioSilence)
{
    effect->processReplacing(effect, (float **)&inSourceP, &inDestP, inFramesToProcess);
}

AEffect *
VSTAU::GetEffect(void)
{
    typedef AEffect *(*EntryPointFunc)(audioMasterCallback audioMaster);
    EntryPointFunc entryPoint;

    entryPoint = (EntryPointFunc)CFBundleGetFunctionPointerForName(bundle, CFSTR("VSTPluginMain"));
    if (!entryPoint)
	entryPoint = (EntryPointFunc)CFBundleGetFunctionPointerForName(bundle, CFSTR("main_macho"));
    if (!entryPoint)
    {
	fprintf(stderr, "Could not find entry point for VSTPluginMain() or main_macho()\n");
	throw(-1004);
    }

    cachedEffect = entryPoint(VSTAU::HostCallback);
    if (!cachedEffect)
    {
	fprintf(stderr, "Could not instantiate effect\n");
	throw(-1005);
    }

    if (cachedEffect->magic != kEffectMagic)
    {
	fprintf(stderr, "Invalid effect magic (0x%08x)\n", cachedEffect->magic);
	throw(-1006);
    }

    cachedEffect->dispatcher(cachedEffect, effOpen, 0, 0, 0, 0);

    return cachedEffect;
}

VstIntPtr VSTCALLBACK
VSTAU::HostCallback(AEffect *effect,
	VstInt32 opcode,
	VstInt32 index,
	VstIntPtr value,
	void *ptr,
	float opt)
{
    switch (opcode)
    {
	case audioMasterVersion:
	    return kVstVersion;

	default:
	    fprintf(stderr, "Host callback opcode %u unimplemented\n", opcode);
	    return 0;
    }
}

#else // 0
/*
*	File:		VSTAU.cpp
*	
*	Version:	1.0
* 
*	Created:	3/18/07
*	
*	Copyright:  Copyright � 2007 __MyCompanyName__, All Rights Reserved
* 
*	Disclaimer:	IMPORTANT:  This Apple software is supplied to you by Apple Computer, Inc. ("Apple") in 
*				consideration of your agreement to the following terms, and your use, installation, modification 
*				or redistribution of this Apple software constitutes acceptance of these terms.  If you do 
*				not agree with these terms, please do not use, install, modify or redistribute this Apple 
*				software.
*
*				In consideration of your agreement to abide by the following terms, and subject to these terms, 
*				Apple grants you a personal, non-exclusive license, under Apple's copyrights in this 
*				original Apple software (the "Apple Software"), to use, reproduce, modify and redistribute the 
*				Apple Software, with or without modifications, in source and/or binary forms; provided that if you 
*				redistribute the Apple Software in its entirety and without modifications, you must retain this 
*				notice and the following text and disclaimers in all such redistributions of the Apple Software. 
*				Neither the name, trademarks, service marks or logos of Apple Computer, Inc. may be used to 
*				endorse or promote products derived from the Apple Software without specific prior written 
*				permission from Apple.  Except as expressly stated in this notice, no other rights or 
*				licenses, express or implied, are granted by Apple herein, including but not limited to any 
*				patent rights that may be infringed by your derivative works or by other works in which the 
*				Apple Software may be incorporated.
*
*				The Apple Software is provided by Apple on an "AS IS" basis.  APPLE MAKES NO WARRANTIES, EXPRESS OR 
*				IMPLIED, INCLUDING WITHOUT LIMITATION THE IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY 
*				AND FITNESS FOR A PARTICULAR PURPOSE, REGARDING THE APPLE SOFTWARE OR ITS USE AND OPERATION ALONE 
*				OR IN COMBINATION WITH YOUR PRODUCTS.
*
*				IN NO EVENT SHALL APPLE BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL OR CONSEQUENTIAL 
*				DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS 
*				OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) ARISING IN ANY WAY OUT OF THE USE, 
*				REPRODUCTION, MODIFICATION AND/OR DISTRIBUTION OF THE APPLE SOFTWARE, HOWEVER CAUSED AND WHETHER 
*				UNDER THEORY OF CONTRACT, TORT (INCLUDING NEGLIGENCE), STRICT LIABILITY OR OTHERWISE, EVEN 
*				IF APPLE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/
#include "VSTAU.h"


//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

COMPONENT_ENTRY(VSTAU)


//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//	VSTAU::VSTAU
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
VSTAU::VSTAU(AudioUnit component)
	: AUEffectBase(component)
{
	CreateElements();
	Globals()->UseIndexedParameters(kNumberOfParameters);
	SetParameter(kParam_One, kDefaultValue_ParamOne );
        
#if AU_DEBUG_DISPATCHER
	mDebugDispatcher = new AUDebugDispatcher (this);
#endif
	
}


//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//	VSTAU::GetParameterValueStrings
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
ComponentResult		VSTAU::GetParameterValueStrings(AudioUnitScope		inScope,
                                                                AudioUnitParameterID	inParameterID,
                                                                CFArrayRef *		outStrings)
{
        
    return kAudioUnitErr_InvalidProperty;
}



//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//	VSTAU::GetParameterInfo
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
ComponentResult		VSTAU::GetParameterInfo(AudioUnitScope		inScope,
                                                        AudioUnitParameterID	inParameterID,
                                                        AudioUnitParameterInfo	&outParameterInfo )
{
	ComponentResult result = noErr;

	outParameterInfo.flags = 	kAudioUnitParameterFlag_IsWritable
						|		kAudioUnitParameterFlag_IsReadable;
    
    if (inScope == kAudioUnitScope_Global) {
        switch(inParameterID)
        {
            case kParam_One:
                AUBase::FillInParameterName (outParameterInfo, kParameterOneName, false);
                outParameterInfo.unit = kAudioUnitParameterUnit_LinearGain;
                outParameterInfo.minValue = 0.0;
                outParameterInfo.maxValue = 1;
                outParameterInfo.defaultValue = kDefaultValue_ParamOne;
                break;
            default:
                result = kAudioUnitErr_InvalidParameter;
                break;
            }
	} else {
        result = kAudioUnitErr_InvalidParameter;
    }
    


	return result;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//	VSTAU::GetPropertyInfo
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
ComponentResult		VSTAU::GetPropertyInfo (AudioUnitPropertyID	inID,
                                                        AudioUnitScope		inScope,
                                                        AudioUnitElement	inElement,
                                                        UInt32 &		outDataSize,
                                                        Boolean &		outWritable)
{
	return AUEffectBase::GetPropertyInfo (inID, inScope, inElement, outDataSize, outWritable);
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//	VSTAU::GetProperty
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
ComponentResult		VSTAU::GetProperty(	AudioUnitPropertyID inID,
                                                        AudioUnitScope 		inScope,
                                                        AudioUnitElement 	inElement,
                                                        void *			outData )
{
	return AUEffectBase::GetProperty (inID, inScope, inElement, outData);
}


#pragma mark ____VSTAUEffectKernel


//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//	VSTAU::VSTAUKernel::Reset()
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void		VSTAU::VSTAUKernel::Reset()
{
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//	VSTAU::VSTAUKernel::Process
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void VSTAU::VSTAUKernel::Process(	const Float32 	*inSourceP,
                                                    Float32		 	*inDestP,
                                                    UInt32 			inFramesToProcess,
                                                    UInt32			inNumChannels, // for version 2 AudioUnits inNumChannels is always 1
                                                    bool			&ioSilence )
{

	//This code will pass-thru the audio data.
	//This is where you want to process data to produce an effect.
	
	UInt32 nSampleFrames = inFramesToProcess;
	const Float32 *sourceP = inSourceP;
	Float32 *destP = inDestP;
	Float32 gain = 1.0 - GetParameter( kParam_One );
		
	while (nSampleFrames-- > 0) {
		Float32 inputSample = *sourceP;
		
		//The current (version 2) AudioUnit specification *requires* 
	    //non-interleaved format for all inputs and outputs. Therefore inNumChannels is always 1
		
		sourceP += inNumChannels;	// advance to next frame (e.g. if stereo, we're advancing 2 samples);
									// we're only processing one of an arbitrary number of interleaved channels

			// here's where you do your DSP work
                Float32 outputSample = inputSample * gain;
            		
		*destP = outputSample;
		destP += inNumChannels;
	}
}
#endif // 0

