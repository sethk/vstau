//
//  VMMacros.h
//  VSTAU
//
//  Created by Seth Kingsley on 12/7/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#define VMRegisterNotification(observer, selector, name, object)	\
		[[NSNotificationCenter defaultCenter] addObserver:observer selector:@selector(selector) name:name object:object]
