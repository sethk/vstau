//
//  VSTAUCocoaViewFactory.h
//  VSTAU
//
//  Created by Seth Kingsley on 12/25/10.
//  Copyright 2010 Apple Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <AudioUnit/AUCocoaUIView.h>

@interface VSTAUCocoaViewFactory : NSObject <AUCocoaUIBase>
{
}

@end
