//
//  VMPlugInTests.m
//  VSTAU
//
//  Created by Seth Kingsley on 12/31/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import "VMPlugInTests.h"
#import	"VMPlugIn.h"

static const NSString	*kAGainPlugInGlob = @"~/Library/Audio/Plug-Ins/VST/again.vst";

@implementation VMPlugInTests

- (void)testCreatePlugInFormat
{
	[VMPlugInFormat formatWithNumInputs:2 numOutputs:2];
}

- (void)testInitPlugInFormat
{
	VMPlugInFormat *format = [[VMPlugInFormat alloc] initWithNumInputs:2 numOutputs:2];

	[format release];
}

- (void)testAGain
{
	VMPlugIn *plugIn = [VMPlugIn plugInWithPath:[kAGainPlugInGlob stringByExpandingTildeInPath]];

	STAssertNotNil(plugIn, nil);
	STAssertEquals([plugIn uniqueID], (OSType)('G' << 24 | 'a' << 16 | 'i' << 8 | 'n'), nil);
	STAssertEqualObjects([plugIn productName], @"Gain", nil);
	STAssertEqualObjects([plugIn manufacturerName], @"Steinberg Media Technologies", nil);
}

@end
