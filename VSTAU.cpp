#include <iostream>
#include <mach-o/ldsyms.h>
#include <mach-o/dyld.h>
#if AU_DEBUG_DISPATCHER
#include "AUDebugDispatcher.h"
#endif // AU_DEBUG_DISPATCHER
#include "CAHostTimeBase.h"
#include "VSTLoader.h"
#include "VSTAU.h"
#include <AudioUnit/AudioUnitCarbonView.h>

// Maximum number of VstEvents dispatched per buffer processed:
#define kMaxEventsPerSlice		(256 - 8)	// To make sizeof(mEvents) == 256

// Checks for non-interleaved float sample buffers:
#define CheckAudioBuffer(ab)	assert((ab).mNumberChannels == 1)

using namespace std;

#if AU_DEBUG_DISPATCHER
class VSTAUDebugDispatcher : public AUDebugDispatcher
{
public:
	class AUDD_Locker
	{
	public:
		AUDD_Locker(CAMutex &guard) : mGuard(guard) {didLock = mGuard.Lock();}
		~AUDD_Locker()
		{
			if (didLock)
				mGuard.Unlock();
		}

	private:
		bool		didLock;
		CAMutex		&mGuard;
	};

	// From AUDebugDispatcher.cpp:
	static const char *AUScopeStr(AudioUnitScope inScope)
	{
		switch (inScope)
		{
			case kAudioUnitScope_Global: return "Global";
			case kAudioUnitScope_Output: return "Output";
			case kAudioUnitScope_Input: return "Input";
			case kAudioUnitScope_Group: return "Group";
			default: return NULL;
		}
	}

	// From AUBase.cpp:
	static void PrintAUParamEvent(const AudioUnitParameterEvent &event, FILE* f)
	{
		bool isRamp = event.eventType == kParameterEvent_Ramped;
		fprintf(f, "\tParamID=%ld,Scope=%ld,Element=%ld\n",
				(long)event.parameter, (long)event.scope, (long)event.element);
		fprintf(f, "\tEvent Type:%s,", (isRamp ? "ramp" : "immediate"));
		if (isRamp)
			fprintf(f, "start=%ld,dur=%ld,startValue=%f,endValue=%f\n",
					(long)event.eventValues.ramp.startBufferOffset, (long)event.eventValues.ramp.durationInFrames,
					event.eventValues.ramp.startValue, event.eventValues.ramp.endValue);
		else
			fprintf(f, "start=%ld,value=%f\n", 
					(long)event.eventValues.immediate.bufferOffset,
					event.eventValues.immediate.value);
		fprintf(f, "- - - - - - - - - - - - - - - -\n");
	}

	VSTAUDebugDispatcher(AUBase *au, FILE *file) : AUDebugDispatcher(au, file), mFile(file) {}

	void SetParameter(ComponentResult result,
			AudioUnitParameterID inID, AudioUnitScope inScope, AudioUnitElement inElement,
			Float32 inValue,
			UInt32 inBufferOffsetInFrames)
	{
		fprintf(mFile, "[result %ld] SetParameter()\n", result);
		const char *scopeStr = AUScopeStr(inScope);
		const char *paramStr = "";
		// The output member is private, otherwise we would lock it here.
		if (scopeStr != NULL)
			fprintf(mFile, "\t\t[ID = %ld, %s, Scope = %s, El = %ld, Value = %g, BufOffset = %lu]\n",
					inID, paramStr, scopeStr, inElement, inValue, inBufferOffsetInFrames);
		else
			fprintf(mFile, "\t\t[ID = %ld, %s, Scope = %ld, El = %ld, Value = %g, BufOffset = %lu]\n",
					inID, paramStr, inScope, inElement, inValue, inBufferOffsetInFrames);
		fflush(mFile);
	}

	void ScheduleParameter(ComponentResult result, const AudioUnitParameterEvent *inParameterEvent, UInt32 inNumEvents)
	{
		fprintf(mFile, "[result %ld] ScheduleParameter()\n", result);
		for (UInt32 i = 0; i < inNumEvents; ++i)
			PrintAUParamEvent(inParameterEvent[i], mFile);
		fflush(mFile);
	}

protected:
	FILE *mFile;
};
#endif // AU_DEBUG_DISPATCHER

static const CFStringRef kVersionKey = CFSTR(kAUPresetVersionKey);
static const CFStringRef kTypeKey = CFSTR(kAUPresetTypeKey);
static const CFStringRef kSubTypeKey = CFSTR(kAUPresetSubtypeKey);
static const CFStringRef kManufacturerKey = CFSTR(kAUPresetManufacturerKey);
static const CFStringRef kNameKey = CFSTR(kAUPresetNameKey);
static const CFStringRef kChunkKey = CFSTR(kAUPresetVSTDataKey);

static const SInt32	kSavedStateVersion = 0;

static const float	kMIDIClocksPerBeat = 24;

static CFStringRef gBundleIdentifier = NULL;
static CFBundleRef gVSTBundle = NULL;

COMPONENT_ENTRY(VSTAU);

extern "C" void __attribute__((constructor))
VSTAUInitBundle(void)
{
	// Find our own bundle executable path.  This is some serious h0h0magic�
#ifdef __LP64__
#define MACH_HEADER_TYPE struct mach_header_64
#else
#define MACH_HEADER_TYPE struct mach_header
#endif // __LP64__
	const MACH_HEADER_TYPE *myHeader = &_mh_bundle_header;
	const char *imagePath = NULL;
	int numImages = _dyld_image_count();
	for (int imageIndex = 1; imageIndex < numImages; imageIndex++)
		if (myHeader == (MACH_HEADER_TYPE *)_dyld_get_image_header(imageIndex))
		{
			imagePath = _dyld_get_image_name(imageIndex);
			break;
		}

	// Load our own bundle:
	if (imagePath)
	{
#ifdef DEBUG
		fprintf(stderr, "VSTAU: My bundle image path is %s\n", imagePath);
#endif // DEBUG
		CFURLRef myExecutableURL = CFURLCreateFromFileSystemRepresentation(NULL,
				(const UInt8 *)imagePath,
				strlen(imagePath),
				false);
		CFURLRef myBundleURL = CFURLCreateWithFileSystemPathRelativeToBase(NULL,
				CFSTR("../.."),
				kCFURLPOSIXPathStyle,
				false,
				myExecutableURL);
		CFRelease(myExecutableURL);
		CFBundleRef myBundle = CFBundleCreate(NULL, myBundleURL);
		if (myBundle)
		{
			gBundleIdentifier = (CFStringRef)CFBundleGetValueForInfoDictionaryKey(myBundle, kCFBundleIdentifierKey);
			if (gBundleIdentifier)
				CFRetain(gBundleIdentifier);

			// Look up the plug-in's path in our bundle's properties:
			CFStringRef effectPath = (CFStringRef)CFBundleGetValueForInfoDictionaryKey(myBundle, kVSTAUPlugInPathKey);
			if (effectPath)
			{
				gVSTBundle = NULL;
				CFStringRef errorString;
				ComponentResult result = VSTLoader::LoadEffectBundle(effectPath, gVSTBundle, errorString);
				if (result != noErr)
				{
					fprintf(stderr, "Could not load VST bundle from %s: %s\n",
							CFStringGetCStringPtr(effectPath, kCFStringEncodingMacRoman),
							CFStringGetCStringPtr(errorString, kCFStringEncodingMacRoman));
				}
				CFRelease(effectPath);
			}
			else
				fprintf(stderr, "VSTAU: Missing value for key %s for info dictionary\n",
						CFStringGetCStringPtr(kVSTAUPlugInPathKey, kCFStringEncodingMacRoman));
			CFRelease(myBundle);
		}
		else
			fprintf(stderr, "VSTAU: Cannot load my own bundle URL");
		CFRelease(myBundleURL);
	}
	else
		fprintf(stderr, "VSTAU: Could not find Mach-O bundle path\n");
}

extern "C" void __attribute__((destructor))
VSTAUFinalizeBundle(void)
{
#ifdef DEBUG
	fprintf(stderr, "VSTAU: Finalizing bundle\n");
#endif // DEBUG
	if (gBundleIdentifier)
		CFRelease(gBundleIdentifier);
	if (gVSTBundle)
		VSTLoader::UnloadEffectBundle(gVSTBundle);
}

VSTAU::VSTAU(AudioUnit audioUnit)
	: AUBase(audioUnit, 1, 1),
	AUMIDIBase(this),
	mParamValueStrings(NULL),
	mAudioTimeMutex("VSTAU Audio Time Mutex"),
	mEventsMutex("VSTAU Events Mutex"),
	mPresets(NULL)
{
	CFStringRef errorString;

#if AU_DEBUG_DISPATCHER
	// Create the debug dispatcher:
	if (getenv("AUDebugDispatcher"))
	{
		char logPath[128];
		snprintf(logPath, sizeof(logPath), "/tmp/VSTAU-%d.log", getpid());
		mDebugFile = fopen(logPath, "a");
		mDebugDispatcher = new VSTAUDebugDispatcher(this, mDebugFile);
	}
#endif // AU_DEBUG_DISPATCHER

	// Check to make sure the VST bundle was loaded:
	if (!gBundleIdentifier || !gVSTBundle)
		COMPONENT_THROW(kAudioUnitErr_InvalidPropertyValue);

	// Open the effect:
	ComponentResult result;
	if ((result = VSTLoader::CreateEffect(gVSTBundle,
			VSTAU::HostCallback,
			this,
			effect,
			errorString)) != noErr)
	{
		clog << "VSTAU: " << CFStringGetCStringPtr(errorString, kCFStringEncodingMacRoman) << endl;
		COMPONENT_THROW(result);
	}

	// Create a parameter element for each effect parameter:
	Globals()->UseIndexedParameters(effect->numParams);

	// Cache the parameter base and scaling values:
	mParamInfo = new VSTAUParameterInfo[effect->numParams];
	for (SInt32 i = 0; i < effect->numParams; ++i)
	{
		VstParameterProperties paramProps;

		if (effect->dispatcher(effect, effGetParameterProperties, i, 0, &paramProps, 0) &&
				paramProps.flags & kVstParameterUsesIntegerMinMax)
		{
			mParamInfo[i].minValue = paramProps.minInteger;
			mParamInfo[i].valueScale = 1.0 / (paramProps.maxInteger - paramProps.minInteger);
		}
		else
		{
			mParamInfo[i].minValue = 0.0;
			mParamInfo[i].valueScale = 1.0;
		}
	}

	// Find our own bundle:
	CFBundleRef plugInBundle = CFBundleGetBundleWithIdentifier(gBundleIdentifier);
	if (!plugInBundle)
		COMPONENT_THROW(kAudioUnitErr_InvalidPropertyValue);

	// Load the parameter value strings if they exist:
	CFURLRef paramStringsURL = CFBundleCopyResourceURL(plugInBundle, kVSTAUParamStringsName, CFSTR("plist"), NULL);
	if (paramStringsURL)
	{
		CFDataRef paramStringsData;
		SInt32 errorCode;
		if (CFURLCreateDataAndPropertiesFromResource(NULL, paramStringsURL, &paramStringsData, NULL, NULL, &errorCode))
		{
			if (!(mParamValueStrings = (CFArrayRef)CFPropertyListCreateFromXMLData(NULL,
					paramStringsData,
					kCFPropertyListImmutable,
					&errorString)))
				clog << "VSTAU: Could not parse parameter values XML: " <<
						CFStringGetCStringPtr(errorString, kCFStringEncodingMacRoman) << endl;
			CFRelease(paramStringsData);
		}
		CFRelease(paramStringsURL);
	}

	// Create an event list for processing:
	mEventList = (VstEvents *)new char[sizeof(mEventList->numEvents) + sizeof(mEventList->reserved) +
			sizeof(mEventList->events[0]) * kMaxEventsPerSlice];

	// Create buffer pointers for rendering:
	mInputBuffers = (effect->numInputs) ? new float *[effect->numInputs] : NULL;
	mOutputBuffers = (effect->numOutputs) ? new float *[effect->numOutputs] : NULL;

	ReloadPresets();

	// Sync the initial preset:
	SInt32 programNumber = effect->dispatcher(effect, effGetProgram, 0, 0, 0, 0);
	if (programNumber >= 0 && programNumber < mNumPresets)
		SetAFactoryPresetAsCurrent(mPresets[programNumber]);
}

VSTAU::~VSTAU(void)
{
	// Release objects:
	if (mParamValueStrings)
		CFRelease(mParamValueStrings);
	DeletePresets(mPresets, mNumPresets);
	delete[] mParamInfo;

	// Free processing data:
	delete mEventList;
	if (mInputBuffers)
		delete mInputBuffers;
	if (mOutputBuffers)
		delete mOutputBuffers;

	VSTLoader::DestroyEffect(effect);

#if AU_DEBUG_DISPATCHER
	// Destory the debug dispatcher:
	if (getenv("AUDebugDispatcher"))
	{
		if (mDebugDispatcher)
		{
			fclose(mDebugFile);
			delete mDebugDispatcher;
			mDebugDispatcher = NULL;
		}
	}
#endif // AU_DEBUG_DISPATCHER
}

void
VSTAU::PostConstructor(void)
{
	AUBase::PostConstructor();

	// Configure the IO elements for our channel configuration:
	AUIOElement *ioElement;
	if (effect->numInputs)
	{
		ioElement = GetInput(0);
		CAStreamBasicDescription inputStreamFormat = ioElement->GetStreamFormat();
		if (inputStreamFormat.NumberChannels() != (UInt32)effect->numInputs)
		{
			inputStreamFormat.ChangeNumberChannels(effect->numInputs, false);
			ioElement->SetStreamFormat(inputStreamFormat);
		}
	}
	else
		// If we don't have any inputs, destroy the input bus, lest the host try to configure it:
		Inputs().SetNumberOfElements(0);
	ioElement = GetOutput(0);
	CAStreamBasicDescription outputStreamFormat = ioElement->GetStreamFormat();
	if (outputStreamFormat.NumberChannels() != (UInt32)effect->numOutputs)
	{
		outputStreamFormat.ChangeNumberChannels(effect->numOutputs, false);
		ioElement->SetStreamFormat(outputStreamFormat);
	}

	// Configure the effect with our output format:
	effect->dispatcher(effect, effSetSampleRate, 0, 0, 0, outputStreamFormat.mSampleRate);
	effect->dispatcher(effect, effSetBlockSize, 0, GetMaxFramesPerSlice(), 0, 0);
}

ComponentResult
VSTAU::Initialize(void)
{
	ComponentResult result = AUBase::Initialize();
	if (result != noErr)
		return result;

	// Inform the effect that we are active:
	effect->dispatcher(effect, effMainsChanged, 0, 1, 0, 0);

	return noErr;
}

void
VSTAU::Cleanup(void)
{
	// Inform the effect that we are inactive:
	effect->dispatcher(effect, effMainsChanged, 0, 0, 0, 0);

	AUBase::Cleanup();
}

ComponentResult
VSTAU::GetParameterValueStrings(AudioUnitScope inScope, AudioUnitParameterID inParameterID, CFArrayRef *outStrings)
{
	if (mParamValueStrings && CFArrayGetCount(mParamValueStrings) > (CFIndex)inParameterID)
	{
		CFArrayRef paramStrings = (CFArrayRef)CFArrayGetValueAtIndex(mParamValueStrings, inParameterID);
		
		if (CFArrayGetCount(paramStrings))
		{
			if (outStrings)
				*outStrings = CFArrayCreateCopy(NULL, paramStrings);
			return noErr;
		}
	}
	return kAudioUnitErr_InvalidParameter;
}

void
VSTAU::DeletePresets(AUPreset *presets, SInt32 numPresets)
{
	for (int presetIndex = 0; presetIndex < numPresets; ++presetIndex)
		CFRelease(presets[presetIndex].presetName);
	delete[] presets;
}

bool
VSTAU::ReloadPresets(void)
{
	bool presetsChanged = false;
	AUPreset *newPresets = new AUPreset[effect->numPrograms];

	bool mustIterate = false;
	SInt32 savedProgramIndex;
	for (int programIndex = 0; programIndex < effect->numPrograms; ++programIndex)
	{
		char progName[kVstMaxProgNameLen + 1];
retry:
		if (mustIterate)
		{
			effect->dispatcher(effect, effSetProgram, 0, programIndex, 0, 0);
			effect->dispatcher(effect, effGetProgramName, 0, 0, progName, 0);
		}
		else if (effect->dispatcher(effect, effGetProgramNameIndexed, programIndex, -1, progName, 0) != 1)
		{
			// Can't use effGetProgramNameIndexed, fall back to iterating over the programs:
			savedProgramIndex = effect->dispatcher(effect, effGetProgram, 0, 0, 0, 0);
			mustIterate = true;
			goto retry;
		}

		newPresets[programIndex].presetNumber = programIndex;
		newPresets[programIndex].presetName = CFStringCreateWithCString(NULL, progName, kCFStringEncodingMacRoman);
		if (!presetsChanged &&
				(!mPresets || effect->numPrograms != mNumPresets || CFStringCompare(newPresets[programIndex].presetName,
						 mPresets[programIndex].presetName,
						 0) != kCFCompareEqualTo))
			presetsChanged = true;
	}

	if (mustIterate)
		effect->dispatcher(effect, effSetProgram, 0, savedProgramIndex, 0, 0);

	if (presetsChanged)
	{
		if (mPresets)
			DeletePresets(mPresets, mNumPresets);
		mPresets = newPresets;
		mNumPresets = effect->numPrograms;
	}

	return presetsChanged;
}

ComponentResult
VSTAU::GetPresets(CFArrayRef *outData) const
{
	if (outData)
	{
		CFMutableArrayRef presets = CFArrayCreateMutable(NULL, mNumPresets, NULL);
		for (int presetIndex = 0; presetIndex < mNumPresets; ++presetIndex)
			CFArrayAppendValue(presets, mPresets + presetIndex);
		*outData = presets;
	}
	return noErr;
}

OSStatus
VSTAU::NewFactoryPresetSet(const AUPreset &inNewFactoryPreset)
{
	if (inNewFactoryPreset.presetNumber < 0 || inNewFactoryPreset.presetNumber > mNumPresets)
		return kAudioUnitErr_InvalidParameter;

	effect->dispatcher(effect, effSetProgram, 0, inNewFactoryPreset.presetNumber, 0, 0);
	AUBase::SetAFactoryPresetAsCurrent(inNewFactoryPreset);

	return noErr;
}

ComponentResult
VSTAU::GetParameterInfo(AudioUnitScope inScope,
		AudioUnitParameterID inParameterID,
		AudioUnitParameterInfo &outParameterInfo)
{
	if (inScope == kAudioUnitScope_Global)
	{
		// Set the parameter as R/W by default:
		outParameterInfo.flags = kAudioUnitParameterFlag_IsWritable | kAudioUnitParameterFlag_IsReadable;

		// Get the extended parameter properties:
		VstParameterProperties props;
		bool propsValid = effect->dispatcher(effect, effGetParameterProperties, inParameterID, 0, &props, 0);

		// Retrieve the label:
		if (propsValid)
		{
			AUBase::FillInParameterName(outParameterInfo,
					CFStringCreateWithCString(NULL, props.label, kCFStringEncodingMacRoman),
					true);
		}
		else
		{
			char paramName[32 + 1];
			effect->dispatcher(effect, effGetParamName, inParameterID, 0, paramName, 0);
			AUBase::FillInParameterName(outParameterInfo,
					CFStringCreateWithCString(NULL, paramName, kCFStringEncodingMacRoman),
					true);
			// Some VSTs (LUXONIX: LFX-1310) don't cap their parameter names at kVstMaxParamStrLen:
			if (strlen(paramName) > kVstMaxParamStrLen)
				clog << "VSTAU: Parameter name \"" << paramName << "\" for index " << inParameterID <<
						" is longer than kVstMaxParamStrLen" << endl;
		}

		// Retrieve the unit type:
		if (propsValid && props.flags & kVstParameterIsSwitch)
			outParameterInfo.unit = kAudioUnitParameterUnit_Boolean;
		else
		{
			char unitName[kVstMaxParamStrLen + 1] = {0};
			effect->dispatcher(effect, effGetParamLabel, inParameterID, 0, unitName, 0);
			if (*unitName)
			{
				outParameterInfo.unit = kAudioUnitParameterUnit_CustomUnit;
				outParameterInfo.unitName = CFStringCreateWithCString(NULL, unitName, kCFStringEncodingMacRoman);
			}
			else if (propsValid && props.flags & kVstParameterUsesIntStep)
				outParameterInfo.unit = kAudioUnitParameterUnit_Indexed;
			else
				outParameterInfo.unit = kAudioUnitParameterUnit_Generic;
		}

		// If we detected parameter value strings during install, support them in the AU:
		if (mParamValueStrings &&
				CFArrayGetCount(mParamValueStrings) > (CFIndex)inParameterID &&
				CFArrayGetCount((CFArrayRef)CFArrayGetValueAtIndex(mParamValueStrings, inParameterID)))
			outParameterInfo.flags|= kAudioUnitParameterFlag_ValuesHaveStrings;

		if (propsValid && props.flags & kVstParameterUsesIntegerMinMax)
		{
			outParameterInfo.minValue = props.minInteger;
			outParameterInfo.maxValue = props.maxInteger;
		}
		else
		{
			outParameterInfo.minValue = 0.0f;
			outParameterInfo.maxValue = 1.0f;
		}
		outParameterInfo.defaultValue = outParameterInfo.minValue;

		return noErr;
	}
	else
		return kAudioUnitErr_InvalidParameter;
}

ComponentResult
VSTAU::SaveState(CFPropertyListRef *outData)
{
	// Sync the current preset name, so that it ends up saved with the preset.  Ideally this would be trapped when the
	// CurrentPreset/PresentPreset property was set, but there doesn't seem to be a way to do that:
	AUPreset preset;
	ComponentResult result = AUBase::DispatchGetProperty(kAudioUnitProperty_PresentPreset, kAudioUnitScope_Global, 0,
			&preset);
	if (result != noErr)
	{
		clog << "VSTAU: SaveState(): Cannot get PresentPreset property (code " << result << ")" << endl;
		return result;
	}

	if (preset.presetNumber < 0)
	{
		// User preset; notify the effect of the new program name:
		char programName[kVstMaxProgNameLen + 1];
		CFStringGetCString(preset.presetName, programName, sizeof(programName), kCFStringEncodingMacRoman);
		effect->dispatcher(effect, effSetProgramName, 0, 0, programName, 0);
	}

	if (effect->flags & effFlagsProgramChunks)
	{
		// If the effect uses chunks for programs, embed the chunk into the plist instead of storing all parameters:
		CFMutableDictionaryRef dict = CFDictionaryCreateMutable(NULL, 0,
				&kCFTypeDictionaryKeyCallBacks, &kCFTypeDictionaryValueCallBacks);

		// We must store at minimum the type, subtype, and manufacturer for this preset to be recognized:
		ComponentDescription desc;
		OSErr err = GetComponentInfo((Component)GetComponentInstance(), &desc, NULL, NULL, NULL);
		if (err != noErr)
			return result;
		SaveInteger(dict, kVersionKey, kSavedStateVersion);
		SaveInteger(dict, kTypeKey, desc.componentType);
		SaveInteger(dict, kSubTypeKey, desc.componentSubType);
		SaveInteger(dict, kManufacturerKey, desc.componentManufacturer);
		CFDictionarySetValue(dict, kNameKey, preset.presetName);

		// Retrieve the preset chunk from the effect and store it:
		void *pChunkBytes;
		VstInt32 numChunkBytes = effect->dispatcher(effect, effGetChunk, 1, 0, &pChunkBytes, 0);
		if (numChunkBytes <= 0)
			return kAudioUnitErr_CannotDoInCurrentContext;
		CFDataRef chunk = CFDataCreate(NULL, (const UInt8 *)pChunkBytes, numChunkBytes);
		CFDictionarySetValue(dict, kChunkKey, chunk);
		CFRelease(chunk);

		*outData = dict;
		return noErr;
	}
	else
		return AUBase::SaveState(outData);
}

ComponentResult
VSTAU::RestoreState(CFPropertyListRef inData)
{
	if (effect->flags & effFlagsProgramChunks)
	{
		// If the effect uses chunks for programs, load the chunk from the plist and pass it to the effect instead of
		// loading all parameters:
		CFDictionaryRef dict = static_cast<CFDictionaryRef>(inData);
		CFDataRef chunk = (CFDataRef)CFDictionaryGetValue(dict, kChunkKey);
		if (!chunk)
			return kAudioUnitErr_InvalidFile;
		VstInt32 result = effect->dispatcher(effect, effSetChunk,
				1, CFDataGetLength(chunk), (void *)CFDataGetBytePtr(chunk), 0);
		if (!result)
			return kAudioUnitErr_InvalidFile;

		return noErr;
	}
	else
	{
		ComponentResult result = AUBase::RestoreState(inData);

		if (result == noErr)
		{
			// Synchronize parameters:
			for (VstInt32 parameterIndex = 0; parameterIndex < effect->numParams; ++parameterIndex)
			{
				AudioUnitParameterValue value;
				result = AUBase::GetParameter(parameterIndex, kAudioUnitScope_Global, 0, value);
				if (result == noErr)
				{
					float scaledValue = mParamInfo[parameterIndex].minValue +
							value * mParamInfo[parameterIndex].valueScale;
					if (scaledValue != effect->getParameter(effect, parameterIndex))
						effect->setParameter(effect, parameterIndex, scaledValue);
				}
				else
					break;
			}
		}

		return result;
	}
}

ComponentResult
VSTAU::GetPropertyInfo(AudioUnitPropertyID inID,
		AudioUnitScope inScope,
		AudioUnitElement inElement,
		UInt32 &outDataSize,
		Boolean &outWritable)
{
	switch (inScope)
	{
		case kAudioUnitScope_Global:
			switch (inID)
			{
				case kAudioUnitProperty_BypassEffect:
					outDataSize = sizeof(UInt32);
					outWritable = true;
					return noErr;

				case kAudioUnitProperty_InPlaceProcessing:
					outDataSize = sizeof(UInt32);
					outWritable = true;
					return noErr;

				case kAudioUnitProperty_CocoaUI:
					outDataSize = GetNumCustomCocoaUIViews();
					if (!outDataSize)
						return kAudioUnitErr_InvalidProperty;
					outDataSize*= sizeof(AudioUnitCocoaViewInfo);
					outWritable = false;
					return noErr;

				case kMusicDeviceProperty_InstrumentCount:
					outDataSize = sizeof(UInt32);
					outWritable = false;
					return noErr;

				case kVSTAUProperty_Effect:
					outDataSize = sizeof(effect);
					outWritable = false;
					return noErr;
			}
			break;
	}

	return AUBase::GetPropertyInfo(inID, inScope, inElement, outDataSize, outWritable);
}

ComponentResult
VSTAU::GetProperty(AudioUnitPropertyID inID, AudioUnitScope inScope, AudioUnitElement inElement, void *outData)
{
	switch (inScope)
	{
		case kAudioUnitScope_Global:
			switch (inID)
			{
				case kAudioUnitProperty_InPlaceProcessing:
					*((UInt32 *)outData) = (UInt32)mProcessInPlace;
					return noErr;

				case kAudioUnitProperty_BypassEffect:
					*((UInt32 *)outData) = (UInt32)mIsBypassed;
					return noErr;

				case kAudioUnitProperty_CocoaUI:
					return GetCustomCocoaUIViewInfos((AudioUnitCocoaViewInfo *)outData);
					
				case kMusicDeviceProperty_InstrumentCount:
					*((UInt32 *)outData) = 0;
					return noErr;

				case kVSTAUProperty_Effect:
					*((AEffect **)outData) = effect;
					return noErr;
			}
			break;
	}

	return AUBase::GetProperty(inID, inScope, inElement, outData);
}

ComponentResult
VSTAU::SetProperty(AudioUnitPropertyID inID,
		AudioUnitScope inScope,
		AudioUnitElement inElement,
		const void *inData,
		UInt32 inDataSize)
{
	switch (inScope)
	{
		case kAudioUnitScope_Global:
			switch (inID)
			{
				case kAudioUnitProperty_InPlaceProcessing:
					if (inDataSize != sizeof(UInt32))
						return kAudioUnitErr_InvalidParameter;
					mProcessInPlace = (bool)*((UInt32 *)inData);
					return noErr;

				case kAudioUnitProperty_BypassEffect:
					if (inDataSize != sizeof(UInt32))
						return kAudioUnitErr_InvalidProperty;
					SetBypassEffect((bool)*((UInt32 *)inData));
					return noErr;
			}
			break;
	}

	return AUBase::SetProperty(inID, inScope, inElement, inData, inDataSize);
}

ComponentResult
VSTAU::GetParameter(AudioUnitParameterID inID,
		AudioUnitScope inScope,
		AudioUnitElement inElement,
		Float32 &outValue)
{
	if (inScope == kAudioUnitScope_Global)
	{
		outValue = mParamInfo[inID].minValue + effect->getParameter(effect, inID) / mParamInfo[inID].valueScale;
		return noErr;
	}

	return AUBase::GetParameter(inID, inScope, inElement, outValue);
}

ComponentResult
VSTAU::SetParameter(AudioUnitParameterID inID,
		AudioUnitScope inScope,
		AudioUnitElement inElement,
		Float32 inValue,
		UInt32 inBufferOffsetInFrames)
{
	if (inScope == kAudioUnitScope_Global)
		effect->setParameter(effect, inID, (inValue - mParamInfo[inID].minValue) * mParamInfo[inID].valueScale);

	ComponentResult result = AUBase::SetParameter(inID, inScope, inElement, inValue, inBufferOffsetInFrames);
#if AU_DEBUG_DISPATCHER
	if (mDebugDispatcher)
		(static_cast<VSTAUDebugDispatcher *>(mDebugDispatcher))->SetParameter(result,
				inID, inScope, inElement, inValue, inBufferOffsetInFrames);
#endif // AU_DEBUG_DISPATCHER
	return result;
}

ComponentResult
VSTAU::ScheduleParameter(const AudioUnitParameterEvent *inParameterEvent, UInt32 inNumEvents)
{
	ComponentResult result = AUBase::ScheduleParameter(inParameterEvent, inNumEvents);
#if AU_DEBUG_DISPATCHER
	if (mDebugDispatcher)
		(static_cast<VSTAUDebugDispatcher *>(mDebugDispatcher))->ScheduleParameter(result, inParameterEvent, inNumEvents);
#endif // AU_DEBUG_DISPATCHER
	return result;
}

ComponentResult
VSTAU::Render(AudioUnitRenderActionFlags &ioActionFlags, const AudioTimeStamp &inTimeStamp, UInt32 inNumberFrames)
{
	// Store the time-stamp for host callbacks:
	mAudioTimeMutex.Lock();
	mAudioTimeStamp = inTimeStamp;
	mAudioTimeValid = true;
	mAudioTimeMutex.Unlock();

	// Send any queued events for this buffer:
	mEventList->numEvents = 0;
	mEventList->reserved = 0;
	mEventsMutex.Lock();
	while (!mEventQueue.empty() && mEventList->numEvents < kMaxEventsPerSlice)
	{
		mEventList->events[mEventList->numEvents++] = mEventQueue.front();
		mEventQueue.pop_front();
	}
	mEventsMutex.Unlock();
	if (mEventList->numEvents)
		effect->dispatcher(effect, effProcessEvents, 0, 0, mEventList, 0);

	if (!mIsBypassed || mEffectWillBypass)
	{
		float **inputBuffers;
		AUOutputElement *output = GetOutput(0);

		// If we have input channels, pull the input and setup the buffers:
		if (effect->numInputs)
		{
			if (!HasInput(0))
				return kAudioUnitErr_NoConnection;

			// Pull the input buffer from our connection:
			AUInputElement *input = GetInput(0);
			ComponentResult result = input->PullInput(ioActionFlags, inTimeStamp, 0, inNumberFrames);
			if (result)
				return result;

			// If we are processing in place, use a single set of buffers:
			if (mProcessInPlace && effect->flags & effFlagsCanReplacing)
				output->SetBufferList(input->GetBufferList());
			else
				input->CopyBufferContentsTo(output->GetBufferList());

			// Setup the input buffer pointers as an array:
			AudioBufferList &bufferList = input->GetBufferList();
			for (SInt32 i = 0; i < effect->numInputs; ++i)
			{
				CheckAudioBuffer(bufferList.mBuffers[i]);
				mInputBuffers[i] = (float *)bufferList.mBuffers[i].mData;
			}
			inputBuffers = mInputBuffers;
		}
		else
			// Instruments have no input:
			inputBuffers = NULL;

		// Setup the output buffer pointers as an array:
		AudioBufferList &bufferList = output->GetBufferList();
		for (SInt32 i = 0; i < effect->numOutputs; ++i)
		{
			CheckAudioBuffer(bufferList.mBuffers[i]);
			mOutputBuffers[i] = (float *)bufferList.mBuffers[i].mData;
		}

		// Call the appropriate VST render function:
		if (effect->flags & effFlagsCanReplacing)
			effect->processReplacing(effect, inputBuffers, mOutputBuffers, inNumberFrames);
		else
#if 0
			effect->process(effect, inputBuffers, mOutputBuffers, inNumberFrames);
#else
			assert(!"Non-replacing processing deprecated");
#endif // 0
		return noErr;
	}
	else if (effect->numInputs)
		// If the VST (effect) doesn't support bypass, we copy input to output:
		return CopyBufferLists(GetInput(0)->GetBufferList(), GetOutput(0)->GetBufferList(), inNumberFrames);
	else
		// Bypass on instrument; nothing to do:
		return noErr;
}

UInt32
VSTAU::SupportedNumChannels(const AUChannelInfo **outInfo)
{
	if (outInfo)
	{
		static AUChannelInfo channelInfo;

		channelInfo.inChannels = effect->numInputs;
		channelInfo.outChannels = effect->numOutputs;
		*outInfo = &channelInfo;
	}

	return 1;
}

bool
VSTAU::StreamFormatWritable(AudioUnitScope inScope, AudioUnitElement inElement)
{
	// Only allow the IO format to be changed while we are suspended:
	return IsInitialized() ? false : true;
}

ComponentResult
VSTAU::ChangeStreamFormat(AudioUnitScope inScope, AudioUnitElement inElement,
		const CAStreamBasicDescription &inPrevFormat,
		const CAStreamBasicDescription &inNewFormat)
{
	unsigned numChannels;
	switch (inScope)
	{
		case kAudioUnitScope_Input:
			numChannels = effect->numInputs;
			break;

		case kAudioUnitScope_Output:
			numChannels = effect->numOutputs;
			break;

		default:
			return kAudioUnitErr_InvalidScope;
	}

	if (inNewFormat.NumberChannels() != numChannels ||
			!inNewFormat.IsPCM() ||
			inNewFormat.SampleWordSize() != sizeof(float) ||
			(inNewFormat.mFormatFlags & kAudioFormatFlagsNativeFloatPacked) != kAudioFormatFlagsNativeFloatPacked ||
			(inNewFormat.mFormatFlags & kAudioFormatFlagIsNonInterleaved) != kAudioFormatFlagIsNonInterleaved)
		return kAudioUnitErr_FormatNotSupported;

	ComponentResult result = AUBase::ChangeStreamFormat(inScope, inElement, inPrevFormat, inNewFormat);

	if (result != noErr)
		return result;

	if (inPrevFormat.mSampleRate != inNewFormat.mSampleRate)
	{
		assert(!mInitialized);
		effect->dispatcher(effect, effSetSampleRate, 0, 0, 0, inNewFormat.mSampleRate);
	}

	return noErr;
}

void
VSTAU::SetMaxFramesPerSlice(UInt32 nFrames)
{
	AUBase::SetMaxFramesPerSlice(nFrames);
	assert(!mInitialized);
	effect->dispatcher(effect, effSetBlockSize, 0, nFrames, 0, 0);
}

void
VSTAU::SetBypassEffect(bool inFlag)
{
	mEffectWillBypass = effect->dispatcher(effect, effSetBypass, 0, inFlag, 0, 0);
	mIsBypassed = inFlag;
}

Float64
VSTAU::GetLatency(void)
{
	return (Float64)effect->initialDelay / GetOutput(0)->GetStreamFormat().mSampleRate;
}

bool
VSTAU::SupportsTail(void)
{
	return (effect->dispatcher(effect, effGetTailSize, 0, 0, 0, 0) != 0);
}

Float64
VSTAU::GetTailTime()
{
	VstInt32 tailTime = effect->dispatcher(effect, effGetTailSize, 0, 0, 0, 0);
	assert(tailTime != 0);
	if (tailTime != 1)
		return (Float64)tailTime / GetOutput(0)->GetStreamFormat().mSampleRate;
	else
		return 0;
}

OSStatus
VSTAU::HandleMidiEvent(UInt8 inStatus, UInt8 inChannel, UInt8 inData1, UInt8 inData2, UInt32 inStartFrame)
{
	CAMutex::Locker lock(mEventsMutex);

	VstMidiEvent *midiEvent = new VstMidiEvent;
	midiEvent->type = kVstMidiType;
	midiEvent->byteSize = sizeof(*midiEvent);
	midiEvent->deltaFrames = 0;
	midiEvent->flags = 0;
	midiEvent->noteOffset = 0;
	midiEvent->midiData[0] = inStatus & (inChannel - 1);
	midiEvent->midiData[1] = inData1;
	midiEvent->midiData[2] = inData2;
	midiEvent->midiData[3] = 0;
	midiEvent->detune = 0;
	midiEvent->noteOffVelocity = 0; // TODO?
	midiEvent->reserved1 = midiEvent->reserved2 = 0;
	mEventQueue.push_back((VstEvent *)midiEvent);

	return noErr;
}

#if !__x86_64__
int
VSTAU::GetNumCustomUIComponents(void)
{
	return (effect->flags & effFlagsHasEditor) ? 1 : 0;
}

void
VSTAU::GetUIComponentDescs(ComponentDescription *inDescArray)
{
	assert(effect->flags & effFlagsHasEditor);
	inDescArray[0].componentType = kAudioUnitCarbonViewComponentType;

	// Get our component subtype (uniqueID):
	ComponentDescription desc;
	OSErr err = GetComponentInfo((Component)GetComponentInstance(), &desc, NULL, NULL, NULL);
	if (err != noErr)
	{
		clog << "VSTAU: Cannot get component info (" << err << ')' << endl;
		COMPONENT_THROW(kAudioUnitErr_CannotDoInCurrentContext);
	}

	inDescArray[0].componentSubType = desc.componentSubType;
	inDescArray[0].componentManufacturer = VSTAU_COMP_MANF;
	inDescArray[0].componentFlags = 0;
	inDescArray[0].componentFlagsMask = 0;
}
#endif // !__x86_64__

int
VSTAU::GetNumCustomCocoaUIViews(void)
{
#if !__x86_64__
	CFBundleRef bundle = CFBundleGetBundleWithIdentifier(gBundleIdentifier);
	if (bundle)
	{
		CFDictionaryRef info = CFBundleGetInfoDictionary(bundle);
		if (info)
		{
			CFBooleanRef boolean;
			if (CFDictionaryGetValueIfPresent(info, kVSTAUHasCocoaUIKey, (const void **)&boolean) &&
					CFGetTypeID(boolean) == CFBooleanGetTypeID() &&
					CFBooleanGetValue(boolean))
			{
#endif // !__x86_64__
				return ((effect->flags & effFlagsHasEditor) == effFlagsHasEditor);
#if !__x86_64__
			}
		}
	}
	return 0;
#endif // !__x86_64__
}

ComponentResult
VSTAU::GetCustomCocoaUIViewInfos(AudioUnitCocoaViewInfo *inInfoArray)
{
	assert(effect->flags & effFlagsHasEditor);
	CFBundleRef bundle = CFBundleGetBundleWithIdentifier(gBundleIdentifier);
	if (bundle)
	{
		CFURLRef bundleURL = CFBundleCopyBundleURL(bundle);
		if (bundleURL)
		{
			inInfoArray[0].mCocoaAUViewBundleLocation = bundleURL;
			inInfoArray[0].mCocoaAUViewClass[0] = kVSTAUCocoaUIClassName;
			return noErr;
		}
		else
		{
			clog << "VSTAU: Cannot get own bundle URL for Cocoa UI" << endl;
			return kAudioUnitErr_CannotDoInCurrentContext;
		}
	}
	else
	{
		clog << "VSTAU: Cannot get own bundle for Cocoa UI" << endl;
		return kAudioUnitErr_CannotDoInCurrentContext;
	}
}

ComponentResult
VSTAU::ComponentEntryDispatch(ComponentParameters *inParams, VSTAU *inVSTAU)
{
	switch (inParams->what)
	{
		case kMusicDeviceMIDIEventSelect:
		case kMusicDeviceSysExSelect:
			return AUMIDIBase::ComponentEntryDispatch(inParams, inVSTAU);
	}

	return AUBase::ComponentEntryDispatch(inParams, inVSTAU);
}

VstIntPtr VSTCALLBACK
VSTAU::HostCallback(AEffect *effect, VstInt32 opcode, VstInt32 index, VstIntPtr value, void *ptr, float opt)
{
	VSTAU *vstau = NULL;
	if (effect)
		vstau = (VSTAU *)effect->user;

	switch (opcode)
	{
		case audioMasterAutomate:
			if (!vstau)
			{
				clog << "VSTAU: audioMasterAutomate called before OpenEffect() completed." << endl;
				return 0;
			}
			vstau->ParameterAutomated(index, opt);
			return 0;

		case audioMasterVersion:
			return kVstVersion;

		case audioMasterIdle:
		{
			SInt32 cond = CFRunLoopRunInMode(kCFRunLoopDefaultMode, 0, false);
			if (cond != kCFRunLoopRunFinished && cond != kCFRunLoopRunTimedOut)
				clog << "VSTAU: unexpected run-loop condition " << cond << endl;
			return 0;
		}

		case audioMasterGetTime:
			return (VstIntPtr)vstau->GetTimeInfo();

		case audioMasterGetVendorString:
		{
			CFBundleRef mainBundle = CFBundleGetMainBundle();
			if (mainBundle)
			{
				CFStringRef vendorString = (CFStringRef)CFBundleGetValueForInfoDictionaryKey(mainBundle,
						CFSTR("NSHumanReadableCopyright"));
				if (!vendorString)
					vendorString = (CFStringRef)CFBundleGetValueForInfoDictionaryKey(mainBundle,
						CFSTR("CFBundleGetInfoString"));
				if (vendorString)
					CFStringGetCString(vendorString, (char *)ptr, kVstMaxVendorStrLen, kCFStringEncodingUTF8);
				else
					snprintf((char *)ptr, kVstMaxVendorStrLen, "%s", "Unknown Vendor");
				return 1;
			}
			else
				return 0;
		}

		case audioMasterGetProductString:
		{
			CFBundleRef mainBundle = CFBundleGetMainBundle();
			if (mainBundle)
			{
				CFStringRef productString =
						(CFStringRef)CFBundleGetValueForInfoDictionaryKey(mainBundle, CFSTR("CFBundleDisplayName"));
				if (productString)
					CFStringGetCString(productString, (char *)ptr, kVstMaxProductStrLen, kCFStringEncodingUTF8);
				else
				{
					CFStringRef bundleName =
							(CFStringRef)CFBundleGetValueForInfoDictionaryKey(mainBundle, CFSTR("CFBundleName"));
					if (bundleName)
						CFStringGetCString(bundleName, (char *)ptr, kVstMaxProductStrLen, kCFStringEncodingUTF8);
					else
					{
						CFURLRef executableURL = CFBundleCopyExecutableURL(mainBundle);
						CFStringRef executableName = CFURLCopyLastPathComponent(executableURL);
						CFStringGetCString(executableName, (char *)ptr, kVstMaxProductStrLen, kCFStringEncodingUTF8);
						CFRelease(executableName);
						CFRelease(executableURL);
					}
				}
				return 1;
			}
			else
				return 0;
		}

		case audioMasterUpdateDisplay:
			if (vstau)
			{
				if (vstau->ReloadPresets()) // Names may have changed, or presets may have been added.
					vstau->PropertyChanged(kAudioUnitProperty_FactoryPresets, kAudioUnitScope_Global, 0);

				// Get the current VST program number and old preset:
				SInt32 programNumber = effect->dispatcher(effect, effGetProgram, 0, 0, 0, 0);
				AUPreset currentPreset;
				ComponentResult result = vstau->DispatchGetProperty(kAudioUnitProperty_PresentPreset,
						kAudioUnitScope_Global,
						0,
						&currentPreset);
				assert(result == noErr);

				// Set the corresponding AUPreset:
				if (programNumber >= 0 && programNumber < vstau->mNumPresets &&
						(currentPreset.presetNumber != programNumber ||
						 CFStringCompare(currentPreset.presetName,
								vstau->mPresets[programNumber].presetName,
								0) !=  kCFCompareEqualTo))
				{
					bool status = vstau->SetAFactoryPresetAsCurrent(vstau->mPresets[programNumber]);
					assert(status);
#ifndef __LP64__
					vstau->PropertyChanged(kAudioUnitProperty_CurrentPreset, kAudioUnitScope_Global, 0);
#endif // !__LP64__
					vstau->PropertyChanged(kAudioUnitProperty_PresentPreset, kAudioUnitScope_Global, 0);
				}
			}
			return 0;

		default:
		{
			string opcodeDesc;
			switch (opcode)
			{
#define OC(n) case audioMaster##n: opcodeDesc = string("audioMaster") + #n; break
OC(CurrentId); OC(PinConnected); OC(WantMidi); OC(ProcessEvents); OC(SetTime); OC(GetNumAutomatableParameters);
OC(GetParameterQuantization); OC(IOChanged); OC(NeedIdle); OC(SizeWindow); OC(GetSampleRate); OC(GetBlockSize);
OC(GetInputLatency); OC(GetOutputLatency); OC(GetPreviousPlug); OC(GetNextPlug); OC(WillReplaceOrAccumulate);
OC(GetCurrentProcessLevel); OC(GetAutomationState); OC(OfflineStart); OC(OfflineRead); OC(OfflineWrite);
OC(OfflineGetCurrentPass); OC(OfflineGetCurrentMetaPass); OC(SetOutputSampleRate); OC(GetOutputSpeakerArrangement);
OC(GetVendorVersion); OC(VendorSpecific); OC(SetIcon); OC(CanDo); OC(GetLanguage); OC(OpenWindow); OC(CloseWindow);
OC(GetDirectory); OC(BeginEdit); OC(EndEdit); OC(OpenFileSelector); OC(CloseFileSelector); OC(EditFile);
OC(GetChunkFile); OC(GetInputSpeakerArrangement);
#undef OC
				default:
					char buf[8];
					snprintf(buf, sizeof(buf), "%d", opcode);
					opcodeDesc = buf;
			}
			clog.setf(ios::dec, ios::basefield);
			clog << "VSTAU: Host callback opcode " << opcodeDesc << " unimplemented" << endl;
			return 0;
		}
	}
}

void
VSTAU::SaveInteger(CFMutableDictionaryRef inDict, const void *key, SInt32 inValue)
{
	CFNumberRef number = CFNumberCreate(NULL, kCFNumberSInt32Type, &inValue);
	CFDictionarySetValue(inDict, key, number);
	CFRelease(number);
}

void
VSTAU::ParameterAutomated(UInt32 inParamID, Float32 inValue)
{
	AUBase::SetParameter(inParamID, kAudioUnitScope_Global, 0, inValue, 0);
}

VstTimeInfo *
VSTAU::GetTimeInfo(void)
{
	CAMutex::Locker lock(mAudioTimeMutex);
	if (mAudioTimeValid)
	{
		static VstTimeInfo timeInfo;
		timeInfo.flags = 0;

		float sampleRate = timeInfo.sampleRate = GetOutput(0)->GetStreamFormat().mSampleRate;

		timeInfo.nanoSeconds = CAHostTimeBase::GetCurrentTimeInNanos();

		Float64 beat, tempo = 0;
		if (CallHostBeatAndTempo(&beat, &tempo) == noErr)
		{
			timeInfo.ppqPos = beat;
			timeInfo.tempo = tempo;
			timeInfo.flags|= kVstPpqPosValid | kVstTempoValid;
		}

		UInt32 offsetToBeat;
		Float32 sigNumerator;
		UInt32 sigDenominator;
		Float64 measureDownBeat;
		if (CallHostMusicalTimeLocation(&offsetToBeat, &sigNumerator, &sigDenominator, &measureDownBeat) == noErr)
		{
			if (beat > -1)
			{
				timeInfo.barStartPos = measureDownBeat;
				timeInfo.flags|= kVstBarsValid;
			}

			timeInfo.timeSigNumerator = sigNumerator;
			timeInfo.timeSigDenominator = sigDenominator;
			timeInfo.flags|= kVstTimeSigValid;

			if (tempo > 0)
			{
				// Quoteth the VST documentation:
				//
				// VstInt32 samplesToNextClock
				//		MIDI Clock Resolution (24 Per Quarter Note), can be negative (nearest clock).
				//
				float samplesPerClock = (sampleRate / (tempo / 60.0)) / kMIDIClocksPerBeat;
				timeInfo.samplesToNextClock = fmod((float)offsetToBeat, samplesPerClock);
				if (timeInfo.samplesToNextClock > samplesPerClock / 2)
					timeInfo.samplesToNextClock = -samplesPerClock + timeInfo.samplesToNextClock;
				timeInfo.flags|= kVstClockValid;
			}
		}

		Boolean isPlaying, stateChanged, isCycling;
		Float64 sampleInTimeLine, cycleStart, cycleEnd;
		if (CallHostTransportState(&isPlaying,
				&stateChanged,
				&sampleInTimeLine,
				&isCycling, &cycleStart, &cycleEnd) == noErr)
		{
			if (stateChanged)
				timeInfo.flags|= kVstTransportChanged;
			if (isPlaying)
				timeInfo.flags|= kVstTransportPlaying;

			timeInfo.samplePos = sampleInTimeLine;

			if (isCycling)
			{
				timeInfo.cycleStartPos = cycleStart;
				timeInfo.cycleEndPos = cycleEnd;
				timeInfo.flags|= kVstTransportCycleActive | kVstCyclePosValid;
			}
		}

		if (mAudioTimeStamp.mFlags & kAudioTimeStampSMPTETimeValid)
		{
			clog << "VSTAU: SMPTE: " << mAudioTimeStamp.mSMPTETime.mCounter << endl;
			timeInfo.smpteOffset = mAudioTimeStamp.mSMPTETime.mCounter;
			timeInfo.smpteFrameRate = mAudioTimeStamp.mSMPTETime.mSubframeDivisor;
			timeInfo.flags|= kVstSmpteValid;
		}

		return &timeInfo;
	}
	else
		return NULL;
}

OSStatus
VSTAU::CopyBufferLists(const AudioBufferList &inBuffer, AudioBufferList &outBuffer, UInt32 inFramesToProcess)
{
	for (unsigned i = 0; i < outBuffer.mNumberBuffers; ++i)
	{
		CheckAudioBuffer(outBuffer.mBuffers[i]);

		if (inBuffer.mNumberBuffers)
		{
			// If we are bypassing processing, we copy source to destination.  If there are N:M channels, we copy
			// destination channels from (channel# % N).  For instance, stereo L,R being copied to 5-channel surround
			// becomes L,R,L,R,L.
			const AudioBuffer &input = inBuffer.mBuffers[i % inBuffer.mNumberBuffers];
			CheckAudioBuffer(input);
			assert(input.mDataByteSize == outBuffer.mBuffers[i].mDataByteSize);
			bcopy(input.mData, outBuffer.mBuffers[i].mData, input.mDataByteSize);
		}
		else
			// TODO: Use ZeroBuffer()?
			// If there are 0:M channels, we just zero the output:
			bzero(outBuffer.mBuffers[i].mData, outBuffer.mBuffers[i].mDataByteSize);
	}

	return noErr;
}
