//
//  VMProgress.h
//  VSTAU
//
//  Created by Seth Kingsley on 12/4/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface VMProgress : NSObject
{
@protected
	float amount;
	NSString *text;
}

+ (VMProgress *)progress;
+ (VMProgress *)progressWithAmount:(float)newAmount text:(NSString *)newText;
- initWithAmount:(float)newAmount text:(NSString *)newText;
- (float)amount;
- (NSString *)text;

@end
