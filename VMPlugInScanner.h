//
//  VMPlugInScanner.h
//  VSTAU
//
//  Created by Seth Kingsley on 12/4/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class VMPlugIn;
@class VMPlugInScanner;

@interface NSObject (VMPlugInScannerDelegate)

- (void)scannerWillBeginScanning:(VMPlugInScanner *)scanner;
- (void)scanner:(VMPlugInScanner *)scanner willScanDirectoryAtPath:(NSString *)dirPath;
- (void)scanner:(VMPlugInScanner *)scanner didScanDirectoryAtPath:(NSString *)dirPath;
- (void)scanner:(VMPlugInScanner *)scanner willScanPlugInAtPath:(NSString *)plugInPath;
- (void)scanner:(VMPlugInScanner *)scanner didScanPlugIn:(VMPlugIn *)plugIn atPath:(NSString *)plugInPath;
- (void)scanner:(VMPlugInScanner *)scanner failedToScanPlugInAtPath:(NSString *)plugInPath;
- (void)scannerDidFinishScanning:(VMPlugInScanner *)scanner;

@end

@interface VMPlugInScanner : NSObject
{
@public
	id delegate;

@protected
	float progressAmount;
	NSMutableArray *scannedPlugIns;
}

// Initializers:
+ (VMPlugInScanner *)scannerWithDelegate:(id)newDelegate;
- initWithDelegate:(id)newDelegate;

// Accessors:
- (float)progressAmount;
- (NSArray *)scannedPlugIns;

// Actions:
- (IBAction)scan:(id)sender;

@end
