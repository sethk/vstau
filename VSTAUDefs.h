#ifndef VSTAUVERSION_H
#define VSTAUVERSION_H

#define kVstVersion2_4	2400

#define kVSTAUVendorString	"Seth Kingsley"
#define kVSTAUProductString	"VSTAU"
#define kVSTAUVersion		0x00000002

#define VSTAU_COMP_SUBTYPE		'Pass'
#define VSTAU_COMP_MANF			'VSTA'

// Note that resource IDs must be spaced 2 apart for the 'STR ' name and description
#define kAudioUnitResID_VSTAU			1000
#define kAudioUnitResID_VSTAUCarbonView	2000

#define kBundleIdentifierFormat	CFSTR("com.meowfishies.VSTAU.%c%c%c%c")
#define kVSTAUPlugInPathKey		CFSTR("VSTAUPlugInPath")
#define kVSTAUHasCocoaUIKey		CFSTR("VSTAUHasCocoaUI")

#define kVSTAUCocoaUIClassName	CFSTR("VSTAUCocoaViewFactory")
#define kVSTAUParamStringsName	CFSTR("ParameterStrings")

#define VSTAUIdleTimerFreq 25

enum VSTAUCustomProperty
{
	kVSTAUProperty_Effect = 64000
};

#endif // VSTAUVERSION_H
