//
//  VSTLoader.cpp
//  VSTAU
//
//  Created by Seth Kingsley on 11/24/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#include <AudioUnit/AudioUnit.h>
#include <CoreMIDI/MIDIServices.h>

#include "VSTLoader.h"
#include <iostream>
#include <mach-o/arch.h>
#include <sys/fcntl.h>
#include <unistd.h>

using namespace std;

#if MAC_OS_X_VERSION_MAX_ALLOWED > MAC_OS_X_VERSION_10_4
#define _BundleCopyExecutableArchitectures CFBundleCopyExecutableArchitectures
#else
#define MAX_ARCHS	8
#include <mach-o/loader.h>
#include <mach-o/fat.h>

static CFArrayRef
_BundleCopyExecutableArchitectures(const CFBundleRef inBundle)
{
	CFURLRef url = CFBundleCopyExecutableURL(inBundle);
	if (!url)
		return NULL;

	UInt8 path[PATH_MAX];
	Boolean status = CFURLGetFileSystemRepresentation(url, true, path, sizeof(path));
	CFRelease(url);
	if (!status)
		return NULL;

	int fd;
	if ((fd = open((const char *)path, O_RDONLY)) == -1)
		return NULL;

	union
	{
		u_int32_t magic;
		u_int32_t dwords[1];
		struct mach_header mh;
		struct mach_header_64 mh64;
		struct
		{
			struct fat_header fh;
#define u_fh	u_fat.fh
			struct fat_arch archs[MAX_ARCHS];
#define u_archs	u_fat.archs
		} u_fat;
	} header;
	ssize_t size = read(fd, &header, sizeof(header));
	close(fd);
	if (size < (ssize_t)sizeof(header.magic))
		return NULL;

	CFArrayRef archs;

	if (header.magic == MH_CIGAM || header.magic == MH_CIGAM_64 || header.magic == FAT_CIGAM)
	{
		ssize_t dwsize = size >> 2;
		for (ssize_t i = 0; i < dwsize; ++i)
			header.dwords[i] = OSSwapInt32(header.dwords[i]);
	}

	if (header.magic == MH_MAGIC || header.magic == MH_MAGIC_64)
	{
		cpu_type_t cpu_type;
		if (header.magic == MH_MAGIC)
			cpu_type = header.mh.cputype;
		else
			cpu_type = header.mh64.cputype;
		CFNumberRef archNum = CFNumberCreate(NULL, kCFNumberSInt32Type, &cpu_type);
		archs = CFArrayCreate(NULL, (const void **)&archNum, 1, &kCFTypeArrayCallBacks);
		CFRelease(archNum);
	}
	else if (header.magic == FAT_MAGIC)
	{
		CFNumberRef archNums[MAX_ARCHS];
		for (unsigned i = 0; i < header.u_fh.nfat_arch; ++i)
			archNums[i] = CFNumberCreate(NULL, kCFNumberSInt32Type, &(header.u_archs[i].cputype));

		archs = CFArrayCreate(NULL, (const void **)archNums, header.u_fh.nfat_arch, &kCFTypeArrayCallBacks);
		for (unsigned i = 0; i < header.u_fh.nfat_arch; ++i)
			CFRelease(archNums[i]);
	}
	else
		return NULL;

	return archs;
}
#endif // MAC_OS_X_VERSION_MAX_ALLOWED > MAC_OS_X_VERSION_10_4

static const CFStringRef kEntryPointName = CFSTR("VSTPluginMain");
static const CFStringRef kOldEntryPointName = CFSTR("main_macho");

ComponentResult
VSTLoader::LoadEffectBundle(const CFStringRef inPath, CFBundleRef &outBundle, CFStringRef &outErrorString)
{
	CFURLRef url = CFURLCreateWithFileSystemPath(NULL, inPath, kCFURLPOSIXPathStyle, false);
	if (!url)
	{
		outErrorString = CFSTR("Invalid URL");
		return kAudioUnitErr_InvalidParameter;
	}
	outBundle = CFBundleCreate(NULL, url);
	CFRelease(url);
	if (!outBundle)
	{
		outErrorString = CFStringCreateWithFormat(NULL, NULL, CFSTR("Could not load bundle at %@"), inPath);
		return kAudioUnitErr_InvalidFile;
	}

	const NXArchInfo *archInfo = NXGetLocalArchInfo();
	if (!archInfo)
	{
		outErrorString = CFSTR("Could not get architecture info");
		CFRelease(outBundle);
		return kAudioUnitErr_FailedInitialization;
	}
	CFNumberRef currentArchNum = CFNumberCreate(NULL, kCFNumberSInt32Type, &(archInfo->cputype));
	CFArrayRef archs = _BundleCopyExecutableArchitectures(outBundle);
	if (!archs)
	{
		outErrorString = CFSTR("Could not get bundle executable architectures");
		CFRelease(outBundle);
		return kAudioUnitErr_UnknownFileType;
	}
	Boolean supportedArch = CFArrayContainsValue(archs, CFRangeMake(0, CFArrayGetCount(archs)), currentArchNum);
	CFRelease(currentArchNum);
	CFRelease(archs);
	if (!supportedArch)
	{
		outErrorString = CFSTR("Architecture not supported");
		CFRelease(outBundle);
		return kAudioUnitErr_FormatNotSupported;
	}

	if (!CFBundleIsExecutableLoaded(outBundle) && !CFBundleLoadExecutable(outBundle))
	{
		outErrorString = CFSTR("Could not load bundle executable");
		CFRelease(outBundle);
		return kAudioUnitErr_InvalidFile;
	}

	return noErr;
}

ComponentResult
VSTLoader::CreateEffect(CFBundleRef inBundle, 
		audioMasterCallback inHostCallback,
		void *inUserData,
		AEffect *&outEffect,
		CFStringRef &outErrorString)
{
	typedef AEffect *(*EntryPointFunc)(audioMasterCallback audioMaster);
	EntryPointFunc entryPoint;

	entryPoint = (EntryPointFunc)CFBundleGetFunctionPointerForName(inBundle, kEntryPointName);
	if (!entryPoint)
		entryPoint = (EntryPointFunc)CFBundleGetFunctionPointerForName(inBundle, kOldEntryPointName);
	if (!entryPoint)
	{
		outErrorString = CFStringCreateWithFormat(NULL, NULL, CFSTR("Could not find entry point for %@() or %@()"),
				kEntryPointName, kOldEntryPointName);
		return kAudioUnitErr_UnknownFileType;
	}

	outEffect = entryPoint(inHostCallback);
	if (!outEffect)
	{
		outErrorString = CFSTR("Could not instantiate effect");
		return kAudioUnitErr_FailedInitialization;
	}

	if (outEffect->magic != kEffectMagic)
	{
		outErrorString = CFStringCreateWithFormat(NULL, NULL, CFSTR("Invalid effect magic (0x%08x)"), outEffect->magic);
		return kAudioUnitErr_FormatNotSupported;
	}

	// Set the user pointer for callbacks:
	outEffect->user = inUserData;

	// Open the effect:
	outEffect->dispatcher(outEffect, effOpen, 0, 0, 0, 0);

	return noErr;
}

void
VSTLoader::DestroyEffect(AEffect *inEffect)
{
	// IK Multimedia: SampleTank 2.x doesn't synchronize its MIDI thread initialization, so we can end up crashing if we
	// open and then close the effect too quickly.  Flushing the MIDI queue tries to work around this problem by using
	// the CoreMIDI thread synchronization instead:
	if (inEffect->uniqueID == (OSType)('1' << 24 | '2' << 16 | 'T' << 8 | 'S'))
		MIDIFlushOutput(NULL);

	// Close the effect:
	inEffect->dispatcher(inEffect, effClose, 0, 0, 0, 0);
}

void
VSTLoader::UnloadEffectBundle(CFBundleRef inBundle)
{
	// If we are the last one using the bundle, unload the executable:
	if (CFGetRetainCount(inBundle) == 1)
	{
#ifdef DEBUG
		clog << "VSTAU: Unloading plug-in executable" << endl;
#endif // DEBUG
		CFBundleUnloadExecutable(inBundle);
	}
	CFRelease(inBundle);
}

ComponentResult
VSTLoader::OpenEffect(const CFStringRef inPath,
		audioMasterCallback inHostCallback,
		void *inUserData,
		AEffect *&outEffect,
		CFBundleRef &outBundle,
		CFStringRef &outErrorString)
{
	ComponentResult result;
	if ((result = VSTLoader::LoadEffectBundle(inPath, outBundle, outErrorString)) != noErr)
		return result;
	if ((result = VSTLoader::CreateEffect(outBundle,
					inHostCallback,
					inUserData,
					outEffect,
					outErrorString)) != noErr)
	{
		VSTLoader::UnloadEffectBundle(outBundle);
		return result;
	}

	return noErr;
}

void
VSTLoader::CloseEffect(AEffect *inEffect, CFBundleRef inBundle)
{
	VSTLoader::DestroyEffect(inEffect);
	VSTLoader::UnloadEffectBundle(inBundle);
}
