//
//  VMPlugIn.mm
//  VSTAU
//
//  Created by Seth Kingsley on 11/24/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

// FIXME:
#include <CoreServices/CoreServices.h>

#import	<AudioUnit/AUComponent.h>
#import	<AudioUnit/AudioUnitCarbonView.h>

#include "VSTLoader.h"
#include "VSTAUDefs.h"

#import	"VMStatusFormatter.h"
#import "VMPlugIn.h"

static const VMStatusFormatter *kStatusFormatter;

@implementation VMPlugInStatusObject

+ (void)initialize
{
	kStatusFormatter = [VMStatusFormatter new];

	[self							setKeys:[NSArray arrayWithObject:@"status"]
  triggerChangeNotificationsForDependentKey:@"statusObject"];
}

+ (NSString *)verbForValue:(enum VMPlugInStatus)value
{
	switch (value)
	{
		case kStatusUninstalled:
			return NSLocalizedString(@"uninstall", nil);

		case kStatusInstalled:
			return NSLocalizedString(@"install", nil);

		default:
			NSAssert1(NO, @"No verb for status %d", value);
			return nil;
	}
}

+ (NSString *)progressiveVerbForValue:(enum VMPlugInStatus)value
{
	switch (value)
	{
		case kStatusUninstalled:
			return NSLocalizedString(@"uninstalling", nil);

		case kStatusInstalled:
			return NSLocalizedString(@"installing", nil);

		default:
			NSAssert1(NO, @"No verb for status %d", value);
			return nil;
	}
}

+ (NSString *)pastTenseVerbForValue:(enum VMPlugInStatus)value
{
	switch (value)
	{
		case kStatusUninstalled:
			return NSLocalizedString(@"uninstalled", nil);

		case kStatusInstalled:
			return NSLocalizedString(@"installed", nil);

		default:
			NSAssert1(NO, @"No verb for status %d", value);
			return nil;
	}
}

+ (VMPlugInStatusObject *)statusObjectWithValue:(enum VMPlugInStatus)newValue
{
	return [[[self alloc] initWithValue:newValue] autorelease];
}

- initWithValue:(enum VMPlugInStatus)newValue
{
	if ((self = [super init]))
	{
		value = newValue;
	}

	return self;
}

- (enum VMPlugInStatus)statusValue
{
	return value;
}

- (NSString *)stringValue
{
	return [kStatusFormatter stringForObjectValue:[NSNumber numberWithInt:value]];
}

- (NSString *)description
{
	return [self stringValue];
}

@end

@implementation VMPlugInFormat

+ (VMPlugInFormat *)formatWithNumInputs:(unsigned)newNumInputs numOutputs:(unsigned)newNumOutputs
{
	return [[[self alloc] initWithNumInputs:newNumInputs numOutputs:newNumOutputs] autorelease];
}

- initWithNumInputs:(unsigned)newNumInputs numOutputs:(unsigned)newNumOutputs
{
	if ((self = [super init]))
	{
		numInputs = newNumInputs;
		numOutputs = newNumOutputs;
	}

	return self;
}

- copyWithZone:(NSZone *)zone
{
	return [self retain];
}

- (NSString *)description
{
	return [self stringValue];
}

- (unsigned)numInputs
{
	return numInputs;
}

- (unsigned)numOutputs
{
	return numOutputs;
}

- (NSString *)stringValue
{
	return [NSString stringWithFormat:NSLocalizedString(@"%u/%u Ch", nil), [self numInputs], [self numOutputs]];
}

- (NSComparisonResult)compare:(VMPlugInFormat *)otherFormat
{
	unsigned selfNumInputs = [self numInputs], otherNumInputs = [otherFormat numInputs];
	if (selfNumInputs != otherNumInputs)
		return (selfNumInputs > otherNumInputs) ? NSOrderedDescending : NSOrderedAscending;
	unsigned selfNumOutputs = [self numOutputs], otherNumOutputs = [otherFormat numOutputs];
	if (selfNumOutputs != otherNumOutputs)
		return (selfNumOutputs > otherNumOutputs) ? NSOrderedDescending : NSOrderedAscending;

	return NSOrderedSame;
}

@end

@interface VMPlugIn (PrivateAPI)

- (void)setMIDI:(BOOL)newIsMIDI;
- (void)examineEffect:(AEffect *)effect;
- (void)setComponentPath:(NSString *)newComponentPath;
- (void)findComponentWithFileName:(NSString *)name;
- (BOOL)checkResourceErrors;
- (BOOL)setString:(NSString *)string inResource:(short)resID;
- (BOOL)setComponentType:(OSType)type subType:(OSType)subType name:(NSString *)name inResource:(short)resID;
- (BOOL)installComponentTemplate;
- (BOOL)deleteComponent;
- (BOOL)updateComponentInfo;
- (short)openComponentResourceFile;
- (BOOL)updateComponentResourceFile:(short)rsrc;
- (BOOL)closeResourceFile:(short)rsrc;
- (BOOL)updateComponentResources;
- (BOOL)updateParameterStrings;

@end

static const NSString	*kVSTAUComponentName = @"VSTAU";
static const NSString	*kComponentExtension = @"component";
static NSString			*kComponentDirPath = nil;
static const NSString	*kComponentDirPathGlob = @"~/Library/Audio/Plug-ins/Components";
static const NSArray	*kComponentSearchPathGlobs =
		[[NSArray alloc] initWithObjects:@"~/Library/Audio/Plug-ins/Components",
				@"/Library/Audio/Plug-ins/Components", nil];
static const NSArray	*kComponentSearchPaths = nil;
static const NSString	*kInfoPListPath = @"Contents/Info.plist";
static const NSString	*kComponentNameFormat = @"%@ (VSTAU)";
static const NSString	*kComponentDescFormat = @"%@ AU";
static const NSString	*kComponentViewDescFormat = @"%@ Custom AU View";
static const NSString	*kUnknownString;
static const OSType		kStringResType = ('S' << 24 | 'T' << 16 | 'R' << 8 | ' ');

static VstIntPtr
hostCallback(AEffect *effect, VstInt32 opcode, VstInt32 index, VstIntPtr value, void *ptr, float opt)
{
	switch (opcode)
	{
		case audioMasterVersion:
			return kVstVersion;

		case audioMasterIdle:
			[[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate date]];
			return 0;

		case audioMasterPinConnected:
			// Return 0 (true) always, for backwards compat.:
			return 0;

		case audioMasterWantMidi:
			[(VMPlugIn *)effect->user setMIDI:YES];
			return 0;

		case audioMasterGetVendorString:
			snprintf((char *)ptr, kVstMaxVendorStrLen, "%s", kVSTAUVendorString);
			return 0;

		case audioMasterGetProductString:
			snprintf((char *)ptr, kVstMaxProductStrLen, "%s", kVSTAUProductString);
			return 0;

		case audioMasterGetVendorVersion:
			return kVSTAUVersion;

		case audioMasterAutomate:
		case audioMasterUpdateDisplay:
			return 0;

		default:
			NSLog(@"Unhandled host callback opcode %d", opcode);
			return 0;
	}
}

@implementation VMPlugIn (PrivateAPI)

- (void)setMIDI:(BOOL)newIsMIDI
{
	isMIDI = newIsMIDI;
}

- (void)examineEffect:(AEffect *)effect
{
	uniqueID = effect->uniqueID;

	char vendor[kVstMaxVendorStrLen + 1];
	if (effect->dispatcher(effect, effGetVendorString, 0, 0, vendor, 0))
		manufacturerName = [[NSString alloc] initWithCString:vendor encoding:NSMacOSRomanStringEncoding];
	else
		manufacturerName = kUnknownString;

	char product[kVstMaxProductStrLen + 1];
	if (effect->dispatcher(effect, effGetProductString, 0, 0, product, 0))
		productName = [[NSString alloc] initWithCString:product encoding:NSMacOSRomanStringEncoding];
	else
		productName = kUnknownString;

	if (!(version = effect->dispatcher(effect, effGetVendorVersion, 0, 0, 0, 0)))
		version = effect->version;

	VSTVersion = effect->dispatcher(effect, effGetVstVersion, 0, 0, 0, 0);

	format = [[VMPlugInFormat alloc] initWithNumInputs:effect->numInputs numOutputs:effect->numOutputs];
	if (VSTVersion >= kVstVersion2_4)	// This may previously have been set from the host callback.
		isMIDI = YES;
}

- (void)setComponentPath:(NSString *)newComponentPath
{
	if (componentPath != newComponentPath)
	{
		[componentPath release];
		componentPath = [newComponentPath copy];
	}
}

- (void)findComponentWithFileName:(NSString *)fileName
{
	NSFileManager *fileManager = [NSFileManager defaultManager];
	NSString *existingComponentFileName = [fileName stringByAppendingPathExtension:kComponentExtension];
	NSEnumerator *enm = [kComponentSearchPaths objectEnumerator];
	NSString *componentSearchPath;
	NSString *installedComponentFileName;
	NSString *installedComponentPath;

	while ((componentSearchPath = [enm nextObject]))
	{
		NSString *existingComponentPath =
				[componentSearchPath stringByAppendingPathComponent:existingComponentFileName];

		if ([fileManager fileExistsAtPath:existingComponentPath])
		{
			[self setComponentPath:existingComponentPath];
			status = kStatusAUExists;
			return;
		}
	}

	installedComponentFileName = [NSString stringWithFormat:kComponentNameFormat, fileName];
	installedComponentPath = [[[kComponentDirPath stringByAppendingPathComponent:installedComponentFileName]
			stringByAppendingPathExtension:kComponentExtension] copy];

	if ([fileManager fileExistsAtPath:installedComponentPath])
		status = kStatusInstalled;
	else
		status = kStatusUninstalled;
	[self setComponentPath:installedComponentPath];
}

- (BOOL)checkResourceErrors
{
	OSErr resErr = ResError();
	if (resErr != noErr)
	{
		NSLog(@"Error setting string resource (%d)", resErr);
		return NO;
	}

	OSErr memErr = MemError();
	if (memErr != noErr)
	{
		NSLog(@"Memory error while setting string resource (%d)", memErr);
		return NO;
	}

	return YES;
}

- (BOOL)setString:(NSString *)string inResource:(short)resID
{
	StringHandle stringHandle = (StringHandle)Get1Resource(kStringResType, resID);
	if (!stringHandle)
	{
		NSLog(@"Could not get string resource with ID %hd", resID);
		return NO;
	}

	unsigned char pstr[256];
	CFStringGetPascalString((CFStringRef)string, pstr, sizeof(pstr), kCFStringEncodingMacRoman);

	SetString(stringHandle, pstr);
	if (![self checkResourceErrors])
		return NO;

	SetResourceSize((Handle)stringHandle, pstr[0]);

	ChangedResource((Handle)stringHandle);

	return YES;
}

- (BOOL)setComponentType:(OSType)type subType:(OSType)subType name:(NSString *)name inResource:(short)resID
{
	Handle descHandle = GetResource(kComponentResourceType, resID);
	if (!descHandle)
	{
		NSLog(@"Could not get handle for resource with ID %hd", resID);
		return NO;
	}

	unsigned char pstr[256];
	CFStringGetPascalString((CFStringRef)name, pstr, sizeof(pstr), kCFStringEncodingMacRoman);

	HLock((Handle)descHandle);

	ComponentDescription *pDesc = (ComponentDescription *)*descHandle;
	pDesc->componentType = type;
	pDesc->componentSubType = subType;

	SetResInfo((Handle)descHandle, resID, pstr);

	ChangedResource((Handle)descHandle);

	HUnlock((Handle)descHandle);

	return YES;
}

- (BOOL)installComponentTemplate
{
	NSFileManager *fileManager = [NSFileManager defaultManager];
	NSBundle *mainBundle = [NSBundle mainBundle];
	NSString *componentTemplate = [mainBundle pathForResource:kVSTAUComponentName ofType:kComponentExtension];

	// Copy the template into the AU directory:
	NSString *theComponentPath = [self componentPath];
	if (![fileManager copyPath:componentTemplate toPath:theComponentPath handler:nil])
	{
		NSLog(@"Could not copy component from %@ to %@", componentTemplate, theComponentPath);
		return NO;
	}

	return YES;
}

- (BOOL)deleteComponent
{
	NSString *theComponentPath = [self componentPath];
	if (![[NSFileManager defaultManager] removeFileAtPath:theComponentPath handler:nil])
	{
		NSLog(@"Could not remove component at %@", theComponentPath);
		return NO;
	}

	return YES;
}

- (BOOL)updateComponentInfo
{
	NSString *infoPath = [[self componentPath] stringByAppendingPathComponent:kInfoPListPath];
	NSMutableDictionary *info = [NSMutableDictionary dictionaryWithContentsOfFile:infoPath];

	if (!info)
	{
		NSLog(@"Could not load bundle info plist at %@", infoPath);
		return NO;
	}

	[info setObject:[self componentBundleIdentifier] forKey:(NSString *)kCFBundleIdentifierKey];
	[info setValue:[self VSTPath] forKey:(NSString *)kVSTAUPlugInPathKey];
	return [info writeToFile:infoPath atomically:YES];
}

- (short)openComponentResourceFile
{
	NSString *theComponentPath = [self componentPath];
	NSBundle *bundle = [NSBundle bundleWithPath:theComponentPath];
	if (!bundle)
	{
		NSLog(@"Could not open bundle at %@", theComponentPath);
		return -1;
	}

	ResFileRefNum rsrc;
	FSRef fs;
	const char *resourcePath = [[bundle pathForResource:@"VSTAU" ofType:@"rsrc"] UTF8String];
	OSStatus result;
	if ((result = FSPathMakeRef((const UInt8 *)resourcePath, &fs, NULL)) != noErr)
	{
		NSLog(@"Could not create FSRef from path %s (%d)", resourcePath, result);
		return -1;
	}
	OSErr err = FSOpenResourceFile(&fs, 0, NULL, fsRdWrPerm, &rsrc);
	if (rsrc == -1)
	{
		NSLog(@"Could not open resource file %s (%d)", resourcePath, err);
		return -1;
	}

	return rsrc;
}

- (BOOL)updateComponentResourceFile:(short)rsrc
{
	NSString *theProductName = [NSString stringWithFormat:kComponentNameFormat, [self productName]];
	NSString *componentName = [NSString stringWithFormat:@"%@: %@", [self manufacturerName], theProductName];
	NSString *componentViewName = [NSString stringWithFormat:@"%@ View", componentName];

	// Update the strings:
	NSString *componentDesc = [NSString stringWithFormat:kComponentDescFormat, theProductName];
	NSString *componentViewDesc = [NSString stringWithFormat:kComponentViewDescFormat, theProductName];
	if (![self setString:componentName inResource:kAudioUnitResID_VSTAU] ||
			![self setString:componentDesc inResource:kAudioUnitResID_VSTAU + 1] ||
			![self setString:componentViewName inResource:kAudioUnitResID_VSTAUCarbonView] ||
			![self setString:componentViewDesc inResource:kAudioUnitResID_VSTAUCarbonView + 1])
		return NO;

	// Update the component types:
	OSType componentType;
	if ([[self format] numInputs] > 0)
		componentType = ([self isMIDI]) ? kAudioUnitType_MusicEffect : kAudioUnitType_Effect;
	else
		componentType = kAudioUnitType_MusicDevice;
	if (![self setComponentType:componentType
						subType:[self uniqueID]
						   name:componentName
					 inResource:kAudioUnitResID_VSTAU] ||
		   ![self setComponentType:kAudioUnitCarbonViewComponentType
						   subType:[self uniqueID]
							  name:componentViewName
						inResource:kAudioUnitResID_VSTAUCarbonView])
		return NO;

	return YES;
}

- (BOOL)closeResourceFile:(short)rsrc
{
	// Flush and close the resource files:
	CloseResFile(rsrc);
	OSErr resError;
	if ((resError = ResError()))
	{
		NSLog(@"Error updating resource file (%d)", resError);
		return NO;
	}

	return YES;
}

- (BOOL)updateComponentResources
{
	short rsrc = [self openComponentResourceFile];
	if (rsrc == -1)
		return NO;

	if (![self updateComponentResourceFile:rsrc] || ![self closeResourceFile:rsrc])
		return NO;

	return YES;
}

- (BOOL)updateParameterStrings
{
	NSString *theVSTPath = [self VSTPath];
	AEffect *effect;
	CFBundleRef VSTBundle;
	NSString *theErrorString;
	CFStringRef cfErrorString = (CFStringRef)theErrorString;

	if (VSTLoader::OpenEffect((CFStringRef)theVSTPath,
			hostCallback, self,
			effect, VSTBundle, cfErrorString) == noErr)
	{
		NSString *theComponentPath = [self componentPath];
		NSBundle *bundle = [NSBundle bundleWithPath:theComponentPath];
		NSString *paramStringsPath =
				[[[bundle resourcePath] stringByAppendingPathComponent:(NSString *)kVSTAUParamStringsName]
										stringByAppendingPathExtension:@"plist"];
		NSMutableArray *paramStrings = [NSMutableArray array];
		VstInt32 paramIndex;
		BOOL hasValueStrings = NO;

		for (paramIndex = 0; paramIndex < effect->numParams; ++paramIndex)
		{
			VstParameterProperties props;
			NSMutableArray *stringsForParam = [NSMutableArray array];

			// Check for enumerated parameters that have string values:
			if (effect->dispatcher(effect, effGetParameterProperties, paramIndex, 0, &props, 0) &&
					(props.flags & kVstParameterUsesIntStep) &&
					props.stepInteger == 1)
			{
				VstInt32 valueLimit = props.maxInteger - props.minInteger;
				VstInt32 valueIndex;

				for (valueIndex = 0; valueIndex <= valueLimit; ++valueIndex)
				{
					char valueString[kVstMaxParamStrLen + 1] = {0};

					effect->setParameter(effect, paramIndex, (float)valueIndex / valueLimit);
					effect->dispatcher(effect, effGetParamDisplay, paramIndex, 0, valueString, 0);
					if (*valueString)
						hasValueStrings = YES;
					[stringsForParam addObject:[NSString stringWithCString:valueString
																  encoding:NSMacOSRomanStringEncoding]];
				}
			}

			[paramStrings addObject:stringsForParam];
		}

		VSTLoader::CloseEffect(effect, VSTBundle);

		if (hasValueStrings && ![paramStrings writeToFile:paramStringsPath atomically:YES])
		{
			NSLog(@"Could not write parameter strings to path %@", paramStringsPath);
			return NO;
		}
	}
	else
	{
		NSLog(@"Could not open effect at path %@: %@", theVSTPath, theErrorString);
		return NO;
	}

	return YES;
}

@end

@implementation VMPlugIn

+ (void)initialize
{
	NSMutableArray *componentSearchPaths = [NSMutableArray array];
	NSEnumerator *enm = [kComponentSearchPathGlobs objectEnumerator];
	NSString *componentSearchPathGlob;

	kComponentDirPath = [[kComponentDirPathGlob stringByExpandingTildeInPath] copy];

	while ((componentSearchPathGlob = [enm nextObject]))
		[componentSearchPaths addObject:[componentSearchPathGlob stringByExpandingTildeInPath]];
	kComponentSearchPaths = [componentSearchPaths copy];

	kUnknownString = NSLocalizedString(@"<Unknown>", nil);
}

+ (VMPlugIn *)plugInWithPath:(NSString *)newPath
{
	return [[[self alloc] initWithPath:newPath] autorelease];
}

- initWithPath:(NSString *)newPath
{
	if ((self = [super init]))
	{
		VSTPath = [newPath copy];
		NSString *VSTFileName = [[newPath lastPathComponent] stringByDeletingPathExtension];

		if ([VSTFileName isEqualToString:@"SH-201 Editor"])
			return nil;

		CFBundleRef bundle;
		AEffect *effect;
		CFStringRef cfErrorString;
		if (VSTLoader::OpenEffect((CFStringRef)newPath,
				hostCallback, self,
				effect, bundle, cfErrorString) == noErr)
		{
			[self examineEffect:effect];
			VSTLoader::CloseEffect(effect, bundle);

			[self findComponentWithFileName:VSTFileName];

			componentBundleIdentifier = [[NSString alloc] initWithFormat:(NSString *)kBundleIdentifierFormat,
					(char)(uniqueID >> 24), (char)(uniqueID >> 16), (char)(uniqueID >> 8), (char)uniqueID];
		}
		else
		{
			status = kStatusError;
			errorString = (NSString *)cfErrorString;
			uniqueID = '?' << 24 | '?' << 16 | '?' << 8 | '?';
			manufacturerName = kUnknownString;
			productName = [VSTFileName copy];
		}
	}

	return self;
}

- (void)dealloc
{
	[errorString release];
	[manufacturerName release];
	[productName release];
	[format release];
	[VSTPath release];
	[componentPath release];
	[componentBundleIdentifier release];

	[super dealloc];
}

- (NSString *)description
{
	return [NSString stringWithFormat:@"%@: %@", [self manufacturerName], [self productName]];
}

- (enum VMPlugInStatus)status
{
	return status;
}

- (void)setStatus:(enum VMPlugInStatus)newStatus
{
	if (status == newStatus || status == kStatusError || status == kStatusAUExists)
		return;

	if (newStatus == kStatusInstalled)
	{
		if (![self installComponentTemplate])
			return;

		BOOL result;
		@try
		{
			result = ([self updateComponentInfo] && [self updateComponentResources] && [self updateParameterStrings]);
			if (!result)
				return;
		}
		@catch (NSException *exception)
		{
			result = NO;
			[exception raise];
		}
		@finally
		{
			if (!result)
				[self deleteComponent];
		}
	}
	else if (newStatus == kStatusUninstalled)
	{
		if (![self deleteComponent])
			return;
	}

	status = newStatus;
}

- (VMPlugInStatusObject *)statusObject
{
	return [VMPlugInStatusObject statusObjectWithValue:[self status]];
}

- (void)setStatusObject:(VMPlugInStatusObject *)statusObject
{
	[self setStatus:[statusObject statusValue]];
}

- (NSArray *)statusValues
{
	if (status == kStatusError || status == kStatusAUExists)
		return [NSArray arrayWithObject:[NSNumber numberWithInt:status]];
	else
		return [NSArray arrayWithObjects:[NSNumber numberWithInt:kStatusUninstalled],
				[NSNumber numberWithInt:kStatusInstalled],
				nil];
}

- (NSArray *)statusObjectValues
{
	NSArray *statusValues = [self statusValues];
	NSEnumerator *enm = [statusValues objectEnumerator];
	NSNumber *statusValue;
	NSMutableArray *statusObjectValues = [NSMutableArray array];

	while ((statusValue = [enm nextObject]))
	{
		VMPlugInStatusObject *statusObject =
				[VMPlugInStatusObject statusObjectWithValue:(enum VMPlugInStatus)[statusValue intValue]];
		[statusObjectValues addObject:statusObject];
	}

	return statusObjectValues;
}

- (NSString *)errorString
{
	return errorString;
}

- (OSType)uniqueID
{
	return uniqueID;
}

- (NSString *)manufacturerName
{
	return manufacturerName;
}

- (NSString *)productName
{
	return productName;
}

- (unsigned)version
{
	return version;
}

- (unsigned)VSTVersion
{
	return VSTVersion;
}

- (BOOL)isMIDI
{
	return isMIDI;
}

- (VMPlugInFormat *)format
{
	return format;
}

- (NSString *)VSTPath
{
	return VSTPath;
}

- (NSString *)abbreviatedVSTPath
{
	return [VSTPath stringByAbbreviatingWithTildeInPath];
}

- (NSString *)componentPath
{
	return componentPath;
}

- (NSString *)componentBundleIdentifier
{
	return componentBundleIdentifier;
}

@end
