//
//  VMAppDelegate.mm
//  VSTAU
//
//  Created by Seth Kingsley on 11/24/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import	"VMPlugIn.h"
#import	"VMPlugInScanner.h"
#import	"VMPlugInsController.h"
#import	"VMMacros.h"
#import "VMAppDelegate.h"

static const NSString *kStatusKey = @"status";
static const NSString *kPlugInsKey = @"plugIns";

@interface VMAppDelegate ()

- (void)showProgressPanel;
- (void)hideProgressPanel;
- (void)progressSheetDidEnd:(NSWindow *)sheet returnCode:(int)returnCode contextInfo:(void *)contextInfo;
- (void)setProgress:(VMProgress *)newProgress;
- (void)updateProgressWithAmount:(float)newAmount format:(NSString *)format, ...;
@property (copy, readwrite, nonatomic) NSString *progressTitle;
- (void)addPlugIn:(VMPlugIn *)plugIn;
- (void)setPlugIns:(NSArray *)newPlugIns;

@end

@implementation VMAppDelegate

- (void)_runStatusChangeThread:(NSDictionary *)arguments;
{
	NSAutoreleasePool *pool = [NSAutoreleasePool new];
	enum VMPlugInStatus status = (enum VMPlugInStatus)[[arguments objectForKey:kStatusKey] intValue];
	NSArray *thePlugIns = [arguments objectForKey:kPlugInsKey];

	[arguments release];
	[pool release];
}

- (void)showProgressPanel
{
	[progressIndicator startAnimation:self];
	[[NSApplication sharedApplication] beginSheet:progressPanel
								   modalForWindow:_managerWindow
									modalDelegate:self
								   didEndSelector:@selector(progressSheetDidEnd:returnCode:contextInfo:)
									  contextInfo:NULL];
}

- (void)hideProgressPanel
{
	[progressIndicator displayIfNeeded];
	[progressIndicator stopAnimation:nil];
	[[NSApplication sharedApplication] performSelector:@selector(endSheet:) withObject:progressPanel afterDelay:0.3];
}

- (void)progressSheetDidEnd:(NSWindow *)sheet returnCode:(int)returnCode contextInfo:(void *)contextInfo
{
	[progressPanel orderOut:nil];
}

- (void)setProgress:(VMProgress *)newProgress
{
	if (progress != newProgress)
	{
		[progress release];
		progress = [newProgress copy];
	}
}

- (void)updateProgressWithAmount:(float)newAmount format:(NSString *)format, ...
{
	va_list arguments;
	va_start(arguments, format);
	NSString *text = [[[NSString alloc] initWithFormat:format arguments:arguments] autorelease];
	VMProgress *newProgress = [VMProgress progressWithAmount:newAmount text:text];
	va_end(arguments);
	[self setProgress:newProgress];
	[progressPanel displayIfNeeded];
	[_managerWindow displayIfNeeded];
}

- (void)addPlugIn:(VMPlugIn *)plugIn
{
	[[self mutableArrayValueForKey:@"plugIns"] addObject:plugIn];
}

- (void)setPlugIns:(NSArray *)newPlugIns
{
    if (plugIns != newPlugIns)
    {
		[plugIns release];
		plugIns = [newPlugIns copy];
    }
}

#pragma mark -
#pragma mark Initialization

- init
{
	if ((self = [super init]))
	{
		[self setProgress:[VMProgress progress]];
		[self setPlugIns:[NSArray array]];
	}

	return self;
}

- (void)dealloc
{
	[progress release];
	[_progressTitle release];
    [plugIns release];

    [super dealloc];
}

- (void)_mainWindowDidBecomeKey:(NSNotification *)notification
{
	if (!_ranFirstRefresh)
	{
		[self refresh:nil];
		_ranFirstRefresh = YES;
	}
}

- (void)awakeFromNib
{
	[(NSNotificationCenter *)[NSNotificationCenter defaultCenter] addObserver:self
																	 selector:@selector(_mainWindowDidBecomeKey:)
																		 name:NSWindowDidBecomeKeyNotification
																	   object:_managerWindow];
}

#pragma mark -
#pragma mark Plug-in Scanner Delegate

- (void)scannerWillBeginScanning:(VMPlugInScanner *)scanner
{
	[self setProgressTitle:NSLocalizedString(@"Scanning Plug-ins…", nil)];
	[self updateProgressWithAmount:[scanner progressAmount] format:NSLocalizedString(@"Preparing…", nil)];
	[self showProgressPanel];
	[[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate date]];
}

- (void)scanner:(VMPlugInScanner *)scanner willScanDirectoryAtPath:(NSString *)dirPath
{
	[self updateProgressWithAmount:[scanner progressAmount] format:NSLocalizedString(@"Searching %@", nil), dirPath];
	[[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate date]];
}

- (void)scanner:(VMPlugInScanner *)scanner willScanPlugInAtPath:(NSString *)plugInPath
{
	[self updateProgressWithAmount:[scanner progressAmount] format:NSLocalizedString(@"Scanning %@", nil), plugInPath];
	[[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate date]];
}

- (void)scanner:(VMPlugInScanner *)scanner didScanPlugIn:(VMPlugIn *)plugIn atPath:(NSString *)plugInPath
{
	[self updateProgressWithAmount:[scanner progressAmount]
							format:NSLocalizedString(@"Found Plug-in %@: %@", nil),
									[plugIn manufacturerName], [plugIn productName]];
	[self addPlugIn:plugIn];
	[[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate date]];
}

- (void)scanner:(VMPlugInScanner *)scanner failedToScanPlugInAtPath:(NSString *)plugInPath
{
	[self updateProgressWithAmount:[scanner progressAmount]
							format:NSLocalizedString(@"Error Scanning %@", nil), plugInPath];
	[[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate date]];
}

- (void)scannerDidFinishScanning:(VMPlugInScanner *)scanner
{
	[self updateProgressWithAmount:1.0 format:NSLocalizedString(@"Finished", nil)];
	[self hideProgressPanel];
}

#pragma mark -
#pragma mark Plug-ins Controller Delegate

- (void)plugInsController:(VMPlugInsController *)controller
			willSetStatus:(enum VMPlugInStatus)status
				ofPlugIns:(NSArray *)changingPlugIns
{
	numChangingPlugIns = [changingPlugIns count];
	numChangedPlugIns = 0;
	[self setProgressTitle:[NSString stringWithFormat:NSLocalizedString(@"%@…", nil),
							[[VMPlugInStatusObject progressiveVerbForValue:status] capitalizedString]]];
	[self updateProgressWithAmount:0.0 format:NSLocalizedString(@"Preparing to %@ plug-ins…", nil),
			[[VMPlugInStatusObject verbForValue:status] capitalizedString]];
	[self showProgressPanel];
}

- (void)plugInsController:(VMPlugInsController *)controller
			willSetStatus:(enum VMPlugInStatus)status
				 ofPlugIn:(VMPlugIn *)plugIn
{
	NSString *verb = [[VMPlugInStatusObject progressiveVerbForValue:status] capitalizedString];

	[self updateProgressWithAmount:(float)numChangedPlugIns++ / numChangingPlugIns
							format:NSLocalizedString(@"%@ %@", nil), verb, plugIn];
}

- (void)plugInsController:(VMPlugInsController *)controller
			 didSetStatus:(enum VMPlugInStatus)status
				ofPlugIns:(NSArray *)plugIns
{
	[self updateProgressWithAmount:1.0 format:NSLocalizedString(@"Finished", nil)];
	[self hideProgressPanel];
}

#pragma mark -
#pragma mark Accessors

- (VMProgress *)progress
{
	return progress;
}

@synthesize progressTitle = _progressTitle;

- (NSArray *)plugIns
{
    return plugIns;
}

#pragma mark -
#pragma mark Actions

- (IBAction)refresh:(id)sender
{
	[self setPlugIns:[NSArray array]];
	VMPlugInScanner *scanner = [VMPlugInScanner scannerWithDelegate:self];
	[scanner scan:sender];
}

- (IBAction)install:(id)sender
{
	[_plugInsController setStatus:kStatusInstalled ofPlugIns:[_plugInsController selectedObjects]];
}

- (IBAction)installAll:(id)sender
{
	[_plugInsController setStatus:kStatusInstalled ofPlugIns:[_plugInsController content]];
}

- (IBAction)uninstall:(id)sender
{
	[_plugInsController setStatus:kStatusUninstalled ofPlugIns:[_plugInsController selectedObjects]];
}

- (IBAction)uninstallAll:(id)sender
{
	[_plugInsController setStatus:kStatusUninstalled ofPlugIns:[_plugInsController content]];
}

@end
