#ifndef VSTAU_H
#define VSTAU_H

#include <list>
#include "AUMIDIBase.h"
#include "CAMutex.h"
#include "VSTAUDefs.h"

struct VSTAUParameterInfo
{
	float minValue;
	float valueScale;
};

class VSTAU : public AUBase, public AUMIDIBase
{
	friend class VSTAUDebugDispatcher;

public:
	VSTAU(AudioUnit audioUnit);
	~VSTAU(void);

	virtual void PostConstructor(void);

	virtual ComponentResult Initialize(void);
	virtual void Cleanup(void);

	virtual ComponentResult GetParameterValueStrings(AudioUnitScope inScope,
			AudioUnitParameterID inParameterID,
			CFArrayRef *outStrings);

	virtual void DeletePresets(AUPreset *presets, SInt32 numPresets);
	virtual bool ReloadPresets(void);
	virtual ComponentResult GetPresets(CFArrayRef *outData) const;
	virtual OSStatus NewFactoryPresetSet(const AUPreset &inNewFactoryPreset);

	virtual ComponentResult GetParameterInfo(AudioUnitScope inScope,
			AudioUnitParameterID inParameterID,
			AudioUnitParameterInfo &outParameterInfo);

	virtual ComponentResult SaveState(CFPropertyListRef *outData);
	virtual ComponentResult RestoreState(CFPropertyListRef inData);

	virtual ComponentResult GetPropertyInfo(AudioUnitPropertyID inID,
			AudioUnitScope inScope,
			AudioUnitElement inElement,
			UInt32 &outDataSize,
			Boolean &outWritable);
	virtual ComponentResult GetProperty(AudioUnitPropertyID inID,
			AudioUnitScope inScope,
			AudioUnitElement inElement,
			void *outData);
	virtual ComponentResult SetProperty(AudioUnitPropertyID inID,
			AudioUnitScope inScope,
			AudioUnitElement inElement,
			const void *inData,
			UInt32 inDataSize);

	virtual ComponentResult GetParameter(AudioUnitParameterID inID,
			AudioUnitScope inScope,
			AudioUnitElement inElement,
			Float32 &outValue);
	virtual ComponentResult SetParameter(AudioUnitParameterID inID,
			AudioUnitScope inScope,
			AudioUnitElement inElement,
			Float32 inValue,
			UInt32 inBufferOffsetInFrames);
	virtual ComponentResult ScheduleParameter(const AudioUnitParameterEvent *inParameterEvent, UInt32 inNumEvents);

	virtual ComponentResult Render(AudioUnitRenderActionFlags &ioActionFlags,
			const AudioTimeStamp &inTimeStamp,
			UInt32 inNumberFrames);

	virtual OSStatus Version(void) {return kVSTAUVersion;}
	virtual UInt32 SupportedNumChannels(const AUChannelInfo **outInfo);
	virtual bool StreamFormatWritable(AudioUnitScope inScope, AudioUnitElement inElement);
	virtual ComponentResult ChangeStreamFormat(AudioUnitScope inScope, AudioUnitElement inElement,
			const CAStreamBasicDescription &inPrevFormat,
			const CAStreamBasicDescription &inNewFormat);
	virtual void SetMaxFramesPerSlice(UInt32 nFrames);
	virtual void SetBypassEffect(bool inFlag);
	virtual Float64 GetLatency(void);
	virtual bool SupportsTail(void);
	virtual Float64 GetTailTime(void);

	OSStatus HandleMidiEvent(UInt8 inStatus, UInt8 inChannel, UInt8 inData1, UInt8 inData2, UInt32 inStartFrame);

#if !__x86_64__
	virtual int GetNumCustomUIComponents(void);
	virtual void GetUIComponentDescs(ComponentDescription *inDescArray);
#endif // !__x86_64__

	virtual int GetNumCustomCocoaUIViews(void);
	virtual ComponentResult GetCustomCocoaUIViewInfos(AudioUnitCocoaViewInfo *inInfoArray);

	static ComponentResult ComponentEntryDispatch(ComponentParameters *inParams, VSTAU *inVSTAU);

protected:
	static VstIntPtr VSTCALLBACK HostCallback(AEffect *effect,
			VstInt32 opcode,
			VstInt32 index,
			VstIntPtr value,
			void *ptr,
			float opt);

	static void SaveInteger(CFMutableDictionaryRef inDict, const void *key, SInt32 inValue);

	virtual void ParameterAutomated(UInt32 inParamID, Float32 inValue);
	virtual VstTimeInfo *GetTimeInfo(void);

	virtual OSStatus CopyBufferLists(const AudioBufferList &inBuffer, AudioBufferList &outBuffer,
			UInt32 inFramesToProcess);

#if AU_DEBUG_DISPATCHER
	FILE *mDebugFile;
#endif // AU_DEBUG_DISPATCHER
	AEffect *effect;
	CFArrayRef mParamValueStrings;
	struct VSTAUParameterInfo *mParamInfo;
	bool mProcessInPlace;
	bool mIsBypassed;
	bool mEffectWillBypass;
	bool mAudioTimeValid;
	AudioTimeStamp mAudioTimeStamp;
	CAMutex mAudioTimeMutex;
	std::list<VstEvent *> mEventQueue;
	CAMutex mEventsMutex;
	VstEvents *mEventList;
	float **mInputBuffers;
	float **mOutputBuffers;
	AUPreset *mPresets;
	SInt32 mNumPresets;
};

#endif // VSTAU_H
