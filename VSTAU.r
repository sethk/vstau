#include <AudioUnit/AudioUnit.r>
#include "VSTAUDefs.h"

// VSTAU:
#define RES_ID			kAudioUnitResID_VSTAU
#define COMP_TYPE		kAudioUnitType_Effect
#define COMP_SUBTYPE	VSTAU_COMP_SUBTYPE
#define COMP_MANUF		VSTAU_COMP_MANF

#define VERSION			kVSTAUVersion
#define NAME			"Seth Kingsley: VSTAU"
#define DESCRIPTION		"VSTAU"
#define ENTRY_POINT		"VSTAUEntry"

#include "AUResources.r"

// VSTAU Carbon View:
#define RES_ID			kAudioUnitResID_VSTAUCarbonView
#define COMP_TYPE		kAudioUnitCarbonViewComponentType
#define COMP_SUBTYPE	VSTAU_COMP_SUBTYPE
#define COMP_MANUF		VSTAU_COMP_MANF

#define VERSION			kVSTAUVersion
#define NAME			"Seth Kingsley: VSTAU View"
#define DESCRIPTION		"VSTAU Custom Carbon AU View"
#define ENTRY_POINT		"VSTAUCarbonViewEntry"

#include "AUResources.r"