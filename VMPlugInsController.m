//
//  VMPlugInsController.m
//  VSTAU
//
//  Created by Seth Kingsley on 12/14/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import	"VMPlugIn.h"
#import "VMPlugInsController.h"

@implementation VMPlugInsController

- (id)delegate
{
	return _delegate;
}

- (void)setDelegate:(id)delegate
{
	_delegate = delegate;
}

- (void)setStatus:(enum VMPlugInStatus)status ofPlugIns:(NSArray *)plugIns
{
	id delegate = [self delegate];
	NSEnumerator *enm = [plugIns objectEnumerator];
	VMPlugIn *plugIn;

	if ([delegate respondsToSelector:@selector(plugInsController:willSetStatus:ofPlugIns:)])
		[delegate plugInsController:self willSetStatus:status ofPlugIns:plugIns];

	while ((plugIn = [enm nextObject]))
	{
		if ([delegate respondsToSelector:@selector(plugInsController:willSetStatus:ofPlugIn:)])
			[delegate plugInsController:self willSetStatus:status ofPlugIn:plugIn];

		[plugIn setStatus:status];

		if ([delegate respondsToSelector:@selector(plugInsController:didSetStatus:ofPlugIn:)])
			[delegate plugInsController:self didSetStatus:status ofPlugIn:plugIn];
	}

	if ([delegate respondsToSelector:@selector(plugInsController:didSetStatus:ofPlugIns:)])
		[delegate plugInsController:self didSetStatus:status ofPlugIns:plugIns];
}

@end
