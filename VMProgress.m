//
//  VMProgress.m
//  VSTAU
//
//  Created by Seth Kingsley on 12/4/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import "VMProgress.h"

@implementation VMProgress

+ (VMProgress *)progress
{
	return [[[self alloc] init] autorelease];
}

+ (VMProgress *)progressWithAmount:(float)newAmount text:(NSString *)newText
{
	return [[[self alloc] initWithAmount:newAmount text:newText] autorelease];
}

- init
{
	return [self initWithAmount:0.0 text:@""];
}

- initWithAmount:(float)newAmount text:(NSString *)newText
{
	if ((self = [super init]))
	{
		amount = newAmount;
		text = [newText copy];
	}

	return self;
}

- (void)dealloc
{
	[text release];

	[super dealloc];
}

- copyWithZone:(NSZone *)zone
{
	return [self retain];
}

- (NSString *)description
{
	return [NSString stringWithFormat:@"%@ -- %.f%%", [self text], [self amount] * 100];
}

- (float)amount
{
	return amount;
}

- (NSString *)text
{
	return text;
}

@end
